<?php
namespace NitroPack;

spl_autoload_register(function ($class) {
    $file = str_replace(array("NitroPack\\", "\\"), array("", DIRECTORY_SEPARATOR), $class) . ".php";
    $path = dirname(__FILE__) . DIRECTORY_SEPARATOR . $file;
    if (file_exists($path)) {
        require_once $path;
    }
});

class HtmlDom {
    public static function fromURL($url) {
        //try to get the url's content with curl if browser class or file_get_contents fails, also try with the browser class
        $dom = new HtmlDom(file_get_contents($url));
        return $dom->getRoot();
    }

    public static function fromFile($file) {
        $dom = new HtmlDom(file_get_contents($file));
        return $dom->getRoot();
    }

    public static function fromString($string) {
        $dom = new HtmlDom($string);
        return $dom->getRoot();
    }

    public static function createElement($tagName) {
        switch (strtolower($tagName)) {
        case 'script':
            return new HtmlDom\ScriptNode('script');
        case 'noscript':
            return new HtmlDom\NoScriptNode('noscript');
        case 'style':
            return new HtmlDom\StyleNode('style');
        default:
            return new HtmlDom\Node(strtolower($tagName));
        }
    }

    private $content;
    private $root;

    public function __construct($content) {
        $this->content = $content;
        $this->root = null;
        $this->buildDom();
    }

    public function getRoot() {
        return $this->root;
    }

    private function buildDom() {
        if (!empty($this->dom)) return;

        $this->root = new HtmlDom\Node('', null);
        $iterator = new HtmlDom\StringIterator($this->content);
        $this->root->parseDom($iterator);
    }
}
