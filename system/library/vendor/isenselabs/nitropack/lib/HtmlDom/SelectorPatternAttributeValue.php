<?php
namespace NitroPack\HtmlDom;

class SelectorPatternAttributeValue {
    public $mode;
    public $value;

    public function __construct($mode, $val) {
        switch ($mode) {
            case '^':
                $this->mode = SelectorPatternMode::STARTS_WITH;
                break;
            case '*':
                $this->mode = SelectorPatternMode::CONTAINS;
                break;
            case '$':
                $this->mode = SelectorPatternMode::ENDS_WITH;
                break;
            default:
                $this->mode = SelectorPatternMode::EQUALS;
                break;
        }

        $this->value = $val;
    }
}
