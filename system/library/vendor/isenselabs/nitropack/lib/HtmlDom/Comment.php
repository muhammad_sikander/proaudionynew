<?php
namespace NitroPack\HtmlDom;

class Comment {
    public $text;

    public function __construct($txt) {
        $this->text = $txt;
    }
}
