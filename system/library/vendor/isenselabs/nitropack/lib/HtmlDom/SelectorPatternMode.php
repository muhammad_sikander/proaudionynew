<?php
namespace NitroPack\HtmlDom;

class SelectorPatternMode {
    const EQUALS = 1;
    const STARTS_WITH = 2;
    const CONTAINS = 3;
    const ENDS_WITH = 4;
}
