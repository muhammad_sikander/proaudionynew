<?php
namespace NitroPack\HtmlDom;

class SvgNode extends Node {
    public function parseDom(&$iterator) {
        $this->containingElement = $this;

        if ($iterator->key() > 0) {
            $iterator->next();
        } 

        foreach ($iterator as $char) {
            if ($char == '<') {
                if (strtolower($iterator->peek(4)) == '/svg') {
                    $iterator->consume(5);
                    $this->readTillClosing($iterator);
                    return;
                }
            }
            $this->innerText .= $char;
        }
    }
}
