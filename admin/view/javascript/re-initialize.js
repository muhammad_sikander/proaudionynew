function reInitialize(category_pri3, category_pri4, category_pri5){
  var create = category_pri3;
  var edit = category_pri4;
  var del = category_pri5;
  
   if(create == 0){
          var disable_create = true;
        }
  if(edit == 0){
          var disable_edit = true;
        }
  if(del == 0){
          var disable_del = true;
        }
    $('#jstree').jstree({
    "core" : {
      "check_callback" : true
    },
    "plugins" : [ "dnd","state" ]
    });
  $.contextMenu({
            selector: '.contextmenu', 
            callback: function(key, options) {
                var m = "clicked: " + key;
            },
            items: {
                "edit": {
            
          name: "Edit name", 
          disabled:disable_edit,
          icon: "edit", 
          accesskey: "e",
          callback: function(key, options) {
            var m = "edit was clicked";
            console.log('id: '+$(this).attr('id'));
            console.log($(this).attr('catname'));
            $(".modal-item-container").html("<div class='form-group'><input class='categoryEdit form-control' type='text' name='' id='"+$(this).attr('id')+"' value='"+$(this).attr('catname')+"'/></div><button class='btn btn-success btn-update-catName'>Update</button>");
            $("#myModal").modal('show');
          }
        },
        
                "Activate/De-Activate": {
                name: "Activate/De-Activate", 
                icon: "cut",
                accesskey: "e",
                callback: function(key, options) {
            console.log('id: '+$(this).attr('id'));
            console.log($(this).text());
            $(".modal-item-container").html("<div class='form-group'><label for='sel1'>Select a Category Status:</label><select class='input-small form-control categorystatus'><option value='0'>Select Status</option><option value='a'>Activate</option><option value='d'>De-Activate</option></select></div><button class='btn btn-success btn-cat-status-update btn-sm'>Update</button><input class='categorystatusupdate' type='hidden' name='' id='"+$(this).attr('id')+"'/>");
            $("#myModal").modal('show');
          }
                },
        
                "copy": {
          name: "Create Category",
          disabled:disable_create, 
                  icon: "copy",
                  accesskey: "e",
                  callback: function(key, options) {
          var m = "Create new category was clicked";
            
            console.log('id: '+$(this).attr('id'));
            console.log($(this).attr('catname'));
            
            $(".modal-item-container").html("<div class='form-group'><label for='name'>Category Name:</label><input class='newcat form-control' type='text' name='' id='"+$(this).attr('id')+"' value=''/></div><button class='btn btn-success btn-newcat'>Submit</button>");
            $("#myModal").modal('show');
          }
                   },
           
        "Delete": {
                name: "Delete", 
                disabled:disable_del,
                icon: "cut",
                accesskey: "e",
                check_callbackk: function(key, options) {
            console.log('id: '+$(this).attr('id'));
            console.log($(this).text());
            
            $(".modal-item-container").html("<label for='sel1'>Confirm category delete</label>&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-success btn-cat-delete btn-sm'>Submit Confirmation</button><input class='categorydelete' type='hidden' name='' id='"+$(this).attr('id')+"'/>");
            $("#myModal").modal('show');
          }
                },
    
                "quit": {name: "Quit", icon: function(){
                    return 'context-menu-icon context-menu-icon-quit';
                }}
            }
        });
  }


    $(document).on("submit","#formclass",function(){
        var select_value = $('#select-items').val();
        var search_value = $('#search').val();
        var tag = '0';
        var pagename= window.location.href;
        var raw_value_check = "1";
        var set_price_check = "0";
        var items_per_page = "0";
        var product_brand = "0";
        if(select_value=="")
          {alert("Please Select an Option");return false;}
        else if(search_value=="")
          {alert("Please Enter Search Value"); return false;}
       
        
         if( select_value =="category_name"){
          
          $('.brands-action').prop('disabled', true);
          $('.tags-action').prop('disabled', true);
          $('.per-page-action').prop('disabled', true);
          $('.price-action').prop('disabled', true);
          $('.grid-list-action').prop('disabled', true);
          $('#search-method').prop('disabled', true);
          $('#featured').prop('disabled', true);

        } else if ( select_value =="item_no"){

          $('.brands-action').prop('disabled', true);
          $('.tags-action').prop('disabled', false);
          $('.per-page-action').prop('disabled', false);
          $('.price-action').prop('disabled', false);
          $('.grid-list-action').prop('disabled', false);
          $('#search-method').prop('disabled', false);
        } else {
          $('.brands-action').prop('disabled', false);
          $('.tags-action').prop('disabled', false);
          $('.per-page-action').prop('disabled', false);
          $('.price-action').prop('disabled', false);
          $('.grid-list-action').prop('disabled', false);
          $('#search-method').prop('disabled', false);

        }

        $.blockUI({ 
        message: 'Please wait, loading products ...', 
        overlayCSS: { backgroundColor: '#808080' } 
      }); 
      
      $.ajax({
        method:"post",
        url:"search_form.php",
        data:"tag="+tag+"&select_value="+select_value+"&search_value="+search_value+"&set_price_check="+set_price_check+"&items_per_page="+items_per_page+"&product_brand="+product_brand+"&raw_value_check="+raw_value_check+"&pagename="+pagename,
        dataType:"json",
        cache:false,
        success:function(panny){
          
           if(panny.items == "" ){
             $("#change-content").html('');
              $("#noData").show();
               $("#toggle-btn").hide();
              $.unblockUI();
          
          } else {
        
          $(".brands-action").html(panny.brand);
          $(".tags-action").html(panny.tag);
          $("#change-content").html(panny.items);
          $(".content-header").hide();
          $("#set-page-show").show();
           $("#noData").hide();

          if (window.matchMedia('(max-width: 767px)').matches)
          {
            $(".skin-blue .main-header .logo").css('width', '76%');
            $("#search").removeAttr('style');
            $("#search").attr('style', 'font-size: 70% !important;height: 33px;background-position: right !important;width: 40% !important;margin-left: 0px !important;padding-left: 10px;background-image: none !important;');
            $("#togglebtn-header").show();
            $("#toggle-btn").show();

         }
           $("#toggle-btn").show();
           $.unblockUI();
          
      }}
   });
       return false;
});

  $(document).on("submit","#advance-search-form",function(){
        var subcat_search_value = $('#subcategory').val();
        var main_category_id = $('#main-category-id').val();
        var raw_value_check = "1";
        var set_price_check = "0";
        var items_per_page = "0";
        var product_brand = "0";
        var tag = "0";
        if(main_category_id==0)
          {alert("Please Select Main Category");return false;}
        else if(subcat_search_value==0)
          {alert("Please Select a Sub Category");return false;}

        $.blockUI({ 
        message: 'Please wait, loading products ...', 
        overlayCSS: { backgroundColor: '#808080' } 
        }); 
      
      $.ajax({
        method:"post",
        url:"search_form.php",
        data:"tag="+tag+"&subcat_search_value="+subcat_search_value+"&sub_session="+subcat_search_value+"&raw_value_check="+raw_value_check+"&set_price_check="+set_price_check+"&items_per_page="+items_per_page+"&product_brand="+product_brand,
        dataType:"json",
        cache:false,
        success:function(pany){
           if(pany.items == ""){
            $("#change-content").html('');
          $("#noData").show();
           $.unblockUI();
          
          }else{
          $(".brands-action").html(pany.brand);
          $(".tags-action").html(pany.tag);
          $("#change-content").html(pany.items);
          $(".content-header").remove();
          $("#set-page-show").show();
            $("#noData").hide();
          if (window.matchMedia('(max-width: 767px)').matches)
          {
           $(".skin-blue .main-header .logo").css('width', '76%');
           $("#togglebtn-header").show();
         }
           $("#toggle-btn").show();
          // $("#toggle-btn").show();
          $("#advance-search-form").hide();
          //alert(pany.items);
          $.unblockUI();
          
        }
        }
      });
      return false;
    });
/////////////////////////////////////////////////////////////////...
$(document).on("click","#clearSearch",function(){
       $('#select-items').val('');
       $('#search').val('');
        $(".brands-action").html('');
        $(".tags-action").html('');
          $("#change-content").html('');
          $(".grid-list-action").val(0);
          $(".content-header").show();
          $("#set-page-show").hide();
          $("#toggle-btn").hide();
          $(".featuredbutton").hide();
  $(".addfeatureproducts").hide();
});
$(document).on("change","#search-method",function(){
        var set_page_value = $(this).val();
        var product_brand = $('#togglebtn-brands').val();
        var tag = $('#togglebtn-tags').val();
        var select_value = $('#select-items').val();
        var search_value = $('#search').val();
        var set_price_check = $("#togglebtn-price").val();
        var items_per_page = $('#togglebtn-per-page').val();
        var subcat_search_value = "0";
        if(set_page_value==0)
          {alert("Please Select Page");return false;}
      $.blockUI({ 
        message: 'Please wait, loading products ...', 
        overlayCSS: { backgroundColor: '#808080' } 
      }); 
      $.ajax({
        method:"post",
        url:"search_form.php",
        data:"tag="+tag+"&set_page_value="+set_page_value+"&set_price_check="+set_price_check+"&select_value="+select_value+"&search_value="+search_value+"&items_per_page="+items_per_page+"&product_brand="+product_brand+"&subcat_search_value="+subcat_search_value,
        dataType:"json",
        cache:false,
        success:function(pany){
           $(".grid-list-action ").prop('disabled', true);
           if(pany.items == ""){
             $("#change-content").html('');

              $("#noData").show();
               $.unblockUI();
          
          }else{
          // $('div.content-wrapper').unblock();
          $(".brands-action").html(pany.brand);
          $(".tags-action").html(pany.tag);
          $("#change-content").html(pany.items);
          $(".content-header").remove();
          //$("#noData").hide();
          //$("#set-page-show").show();
          if (window.matchMedia('(max-width: 767px)').matches)
          {
           $(".skin-blue .main-header .logo").css('width', '76%');
           $("#togglebtn-header").show();

           if(set_page_value == 1)
          {
              $(".improduct").css({'width':'165px !important', 'display':'inline-block !important'});
              $(".fontSize").css('font-size','61%');
              $(".btn-warning").css({'font-size':'61%', 'margin-top':'6px', 'width': '90%','margin-left':'9px'});
              $(".btn-success").css('margin-top','6px');
              $(".addTAg").css('margin','0px 0px 0px 19px');
              $(".tagbtn").css('margin-top','4%');
              $(".statusBtn").css('margin-top','4%');
             
              
          }
         }
           $("#toggle-btn").show();
           $("#noData").hide();
            $.unblockUI();
            
        }}
        
      });
      return false;
    });



//////////////////////////////////////////////////////////////...

$(document).on("change","#togglebtn-price",function(){
        var set_price_check = $(this).val();
        var select_value = $('#select-items').val();
        var search_value = $('#search').val();
         var set_page_value = $('#search-method').val();
        var items_per_page = $('#togglebtn-per-page').val();
        var product_brand = $('#togglebtn-brands').val();
        var tag = $('#togglebtn-tags').val();
        var subcat_search_value = "0";

        if(set_price_check==0)
          {alert("Please Select Page");return false;}
         $.blockUI({ 
        message: 'Please wait, loading products ...', 
        overlayCSS: { backgroundColor: '#808080' } 
      }); 
      $.ajax({
        method:"post",
        url:"search_form.php",
        data:"tag="+tag+"&set_page_value="+set_page_value+"&set_price_check="+set_price_check+"&select_value="+select_value+"&search_value="+search_value+"&items_per_page="+items_per_page+"&product_brand="+product_brand+"&subcat_search_value="+subcat_search_value,
        dataType:"json",
        cache:false,
        success:function(pany){
          // $('div.content-wrapper').unblock();
           if(pany.items == ""){
            $("#change-content").html('');
          $("#noData").show();
           $.unblockUI();
            
          }else{
          $(".brands-action").html(pany.brand);
          $(".tags-action").html(pany.tag);
          $("#change-content").html(pany.items);
          $(".content-header").remove();
           $("#noData").hide();
          //$("#set-page-show").show();
          if (window.matchMedia('(max-width: 767px)').matches)
          {
           $(".skin-blue .main-header .logo").css('width', '76%');
           $("#togglebtn-header").show();
         }
           $("#toggle-btn").show();
             $.unblockUI();
              
        }
        }
      });
      return false;
    });

//////////////////////////////////////////////////////////////

$(document).on("change","#togglebtn-per-page",function(){
        var items_per_page = $(this).val();
        var select_value = $("#select-items").val();
         var set_page_value = $('#search-method').val();
        var search_value = $("#search").val();
        var set_price_check = $("#togglebtn-price").val();
        var product_brand = $('#togglebtn-brands').val();
        var tag = $('#togglebtn-tags').val();
        var subcat_search_value = "0";
        /*alert(product_brand);exit();
        if(set_price_check==null)
          {set_price_check="0";}
        if(set_price_check==null)
          {product_brand="0";}*/
        if(items_per_page=="page")
          {alert("Please Select Page"); return false;}
      $.blockUI({ 
        message: 'Please wait, loading products ...', 
        overlayCSS: { backgroundColor: '#808080' } 
      }); 
      $.ajax({
        method:"post",
        url:"search_form.php",
        data:"tag="+tag+"&set_page_value="+set_page_value+"&items_per_page="+items_per_page+"&select_value="+select_value+"&search_value="+search_value+"&set_price_check="+set_price_check+"&product_brand="+product_brand+"&subcat_search_value="+subcat_search_value,
        dataType:"json",
        cache:false,
        success:function(pany){
          // $('div.content-wrapper').unblock();
          $(".brands-action").html(pany.brand);
          $(".tags-action").html(pany.tag);
          $("#change-content").html(pany.items);
          $(".content-header").remove();
          //$("#set-page-show").show();
          if (window.matchMedia('(max-width: 767px)').matches)
          {
           $(".skin-blue .main-header .logo").css('width', '76%');
           $("#togglebtn-header").show();
         }
           $("#toggle-btn").show();

              $.unblockUI();
          
        }
        
      });
      return false;
    });

///////////////////////////////////////////////////////////////////////////////

$(document).on("change","#togglebtn-brands",function(){
        var product_brand = $(this).val();
        var select_value = $('#select-items').val();
        var search_value = $('#search').val();
        var set_page_value = $('#search-method').val();
        var set_price_check = $("#togglebtn-price").val();
        var items_per_page = $('#togglebtn-per-page').val();
        var tag = $('#togglebtn-tags').val();
        var subcat_search_value = "0";
        if(product_brand==0)
          {alert("Please Select Page");return false;}
         $.blockUI({ 
        message: 'Please wait, loading products ...', 
        overlayCSS: { backgroundColor: '#808080' } 
      }); 
      $.ajax({
        method:"post",
        url:"search_form.php",
        data:"tag="+tag+"&set_page_value="+set_page_value+"&product_brand="+product_brand+"&select_value="+select_value+"&search_value="+search_value+"&set_price_check="+set_price_check+"&items_per_page="+items_per_page+"&subcat_search_value="+subcat_search_value,
        dataType:"json",
        cache:false,
        success:function(pany){
           if(pany.items == ""){
             $("#change-content").html('');
              $("#noData").show();
           $.unblockUI();
          
          }else{
          // $('div.content-wrapper').unblock();
          $(".brands-action").html(pany.brand);
          $(".tags-action").html(pany.tag);
          $("#change-content").html(pany.items);
          $(".content-header").remove();
          $("#noData").hide();
          //$("#set-page-show").show();
          if (window.matchMedia('(max-width: 767px)').matches)
          {
           $(".skin-blue .main-header .logo").css('width', '76%');
           $("#togglebtn-header").show();
         }
           $("#toggle-btn").show();
             $.unblockUI();
          
        }
        }
      });
      return false;
    });
/*================== TAGS SEARCH FUNCTION =====================*/

$(document).on("change","#togglebtn-tags",function(){
        var tag = $(this).val();
         var product_brand = $('#togglebtn-brands').val();
         var tag = $('#togglebtn-tags').val();
        var select_value = $('#select-items').val();
        var search_value = $('#search').val();
        var set_page_value = $('#search-method').val();
        var set_price_check = $("#togglebtn-price").val();
        var items_per_page = $('#togglebtn-per-page').val();
        var subcat_search_value = "0";
        if(tag==0)
          {alert("Please Select tag");return false;}
         $.blockUI({ 
        message: 'Please wait, loading products ...', 
        overlayCSS: { backgroundColor: '#808080' } 
      }); 
      $.ajax({
        method:"post",
        url:"search_form.php",
        data:"tag="+tag+"&set_page_value="+set_page_value+"&product_brand="+product_brand+"&select_value="+select_value+"&search_value="+search_value+"&set_price_check="+set_price_check+"&items_per_page="+items_per_page+"&subcat_search_value="+subcat_search_value,
        dataType:"json",
        cache:false,
        success:function(pany){
           if(pany.items == ""){
             $("#change-content").html('');
              $("#noData").show();
           $.unblockUI();
          
          }else{
          // $('div.content-wrapper').unblock();
          $(".brands-action").html(pany.brand);
          $(".tags-action").html(pany.tag);
          $("#change-content").html(pany.items);
          $(".content-header").remove();
          $("#noData").hide();
          //$("#set-page-show").show();
          if (window.matchMedia('(max-width: 767px)').matches)
          {
           $(".skin-blue .main-header .logo").css('width', '76%');
           $("#togglebtn-header").show();
         }
           $("#toggle-btn").show();
             $.unblockUI();
          
        }
        }
      });
      return false;
    });



//////////////////////////////////////////////////////////////
$(document).ready(function(){

  $("#main-category-id").on('change', function(event){
      var main_select_val = $(this).val();
                  if(main_select_val==0 || main_select_val=="")
                    {   
                        alert("Please Select Category");
                        $("#show-me").hide();
                        return false;
                    }
                    else
                    {   
                        var main_select_val=main_select_val;
                        $("#show-me").show();
                    }

        $.ajax({
        method:"POST",
        url:"search_structure.php",
        data: "main_select_val="+main_select_val,
        dataType:"json",
        cache:false,
        success:function(response){
             $("#subcategory").html(response.subcat);
         }
        
      });
    });

$(".close").on('click', function(event){
  $("#main-category-id").val(0);
  $("#subcategory").val(0);
  $("#show-me").hide();
});

$("#close-check").on('click', function(event){
  $("#main-category-id").val(0);
  $("#subcategory").val(0);
  $("#show-me").hide();
});

$(".grid-list-action").on("change",function(){
  var list_grid=$(this).val();
  if(list_grid==0)
  {
    alert("Please Select an Option");
    return false;
  }
  else if(list_grid==1)
  {
    if (window.matchMedia('(max-width: 767px)').matches)
    {
        $(".changeView").css('width', '100%');
      $(".changeView").css('margin-bottom', '5px');
      $(".changeView").attr("class","col-xs-12 col-lg-12 improduct changeView");
      $(".pricename").attr("class","col-md-0 col-xs-0");
      $(".btnclose").attr("class","col-md-0");
      $(".btnbrand").attr("class","col-md-0");
      $(".textright").hide();
      $(".baseimg").css('float', 'left');
      $(".changeView").css('float', 'left');
      $(".btnclose").css('float', 'left');
      $(".tagbtn").show();
      $(".row.bradname").css('margin-left', '22px');
      $(".row").css('margin-top', '-14px');
      //$(".pid").css('margin-right','28px');
      $(".thumbnail").css('height', '110px');
      $(".Grid").parent().css('margin-left', '154px');
      $(".btn-warning").css('margin-left', '-30px');
      $(".btn-warning").css('margin-top', '-23px');
      $(".btn-success").css({'font-size':'61%','margin-top':'6px'});
   
       $(".ellipsis").css({"margin-left":"15px", "font-size":"61%"});
       $(".pid").css({"margin-right":" 28px", "font-size":"61%"});
       $(".mblTagbtn").removeAttr("style");
       $(".mblTagbtn").attr("style","margin-right: 19px !important");

    
    }
    else
    {
      $(".changeView").css('width', '100%');
      $(".changeView").css('margin-bottom', '5px');
      $(".changeView").attr("class","col-xs-12 col-lg-12 improduct changeView");
      $(".pricename").attr("class","col-md-0 col-xs-0");
      $(".btnclose").attr("class","col-md-0");
      $(".btnbrand").attr("class","col-md-0");
      $(".textright").hide();
      $(".baseimg").css('float', 'left');
      $(".changeView").css('float', 'left');
      $(".btnclose").css('float', 'left');
      $(".tagbtn").show();
      $(".row.bradname").css('margin-left', '22px');
      $(".pid").css('margin-right','28px');
      $(".thumbnail").css('height', '185px');
      $(".btn-warning").css('margin-left', '-2px');
      $(".btn-warning").css('margin-top', '2px');
    }
  }
  else if(list_grid==2)
  {
    if (window.matchMedia('(max-width: 767px)').matches)
    {
      $(".changeView").css('width', '165px');
      $(".changeView").css('margin-bottom', '15px');
      $(".changeView").attr('class', "col-sm-4 col-md-2  improduct changeView");
       $(".changeView1").css('width', '155px');
      $(".changeView1").css('margin-bottom', '15px');
      $(".changeView1").attr('class', "col-sm-4 col-md-2  improduct changeView1");
      $(".btnclose").attr("class","col-md-4 col-sm-4 col-xs-4 !important");
      $(".Grid").parent().attr("class","col-md-4 col-sm-4 col-xs-4 !important","style","margin-left: 27px;");
      $(".Grid").parent().css("margin-left","27px");
      $(".tagbtn").show();
      $(".btnbrand").attr("class","col-md-4 !important");
      $(".textright").show();
      $(".row.bradname").css('margin-left', '0px');
     
      $(".mblTagbtn").removeAttr( 'style' ).attr("style",'margin-top: 9px !important');
      $(".thumbnail").css('height', 'auto');
      $(".ellipsis").parent().attr('class', "col-md-12 col-xs-12 col-sm-12 pricename !important");
      $(".img-responsive").parent().removeAttr( 'style' );
      $(".btn-warning").css('margin-left', '-2px');
      $(".btn-warning").css('margin-top', '10px');
      $(".ellipsis").css({"margin-left":"15px", "font-size":"61%"});
    }
    else
    {
      $(".changeView1").css('width', '155px !important');
      $(".changeView1").css('margin-bottom', '15px');
      $(".changeView1").attr('class', "col-sm-4 col-md-2  improduct changeView1");
      $(".changeView").css('width', '165px');
      $(".changeView").css('margin-bottom', '15px');
      $(".changeView").attr('class', "col-sm-4 col-md-2  improduct changeView");
      $(".btnclose").attr("class","col-md-4 col-sm-4 col-xs-4 !important");
      $(".Grid").parent().attr("class","col-md-4 col-sm-4 col-xs-4 !important");
      $(".tagbtn").show();
      $(".btnbrand").attr("class","col-md-4 !important");
      $(".textright").show();
      $(".row.bradname").css('margin-left', '0px');
      $(".img-responsive").parent().removeAttr( 'style' );
      $(".thumbnail").css('height', 'auto');
      $(".ellipsis").parent().attr('class', "col-md-12 col-xs-12 col-sm-12 pricename !important");
      
      $(".btn-warning").css('margin-left', '-2px');
      $(".btn-warning").css('margin-top', '10px');
    }
  }
});


$(document).on("click",".update_status_Enabled",function(e){
    console.log($(this).attr('pid'));
    var pid = $(this).attr('pid');
    var x = confirm("Are you sure you want to disable?");
      if(x){
      $(".modal-body2").block({ 
        message: 'Please wait, processing ...', 
        overlayCSS: { backgroundColor: '#00f' } ,
        css: { border: '3px solid #a00' } 
      }); 
      
      $.blockUI({ 
        message: 'Disabling the product, please wait ...', 
        overlayCSS: { backgroundColor: '#00f' } 
      }); 
    $.ajax({
      type:"POST",
      url: "disable_single_product.php",
      data:"disableproducts=1&pid="+pid,
      dataType:"json",
      cache:false,
      success:function(rt){
        alert(rt.msg);
        $.unblockUI();
        $(".modal-body2").unblock();
        $("#search-method").val(1).change();
        
      }
    });
    }
  });

  $(document).on("click",".update_status_Disabled",function(e){
    console.log($(this).attr('pid'));
    var pid = $(this).attr('pid');
    var x = confirm("Are you sure you want to enable?");
      if(x){
      $(".modal-body2").block({ 
        message: 'Please wait, processing ...', 
        overlayCSS: { backgroundColor: '#00f' } ,
        css: { border: '3px solid #a00' } 
      }); 
      
      $.blockUI({ 
        message: 'Enabling the product, please wait ...', 
        overlayCSS: { backgroundColor: '#00f' } 
      }); 
    $.ajax({
      type:"POST",
      url: "enable_single_product.php",
      data:"enableproducts=1&pid="+pid,
      dataType:"json",
      cache:false,
      success:function(rt){
        alert(rt.msg);
        $.unblockUI();
        $(".modal-body2").unblock();
        $("#search-method").val(1).change();
        
      }
    });
    }
  });

$(document).on('click','.deleteMainGalleryImage',function(e){
    if(e.handled !== true){
      var pid = $(this).attr('pid');
      var filename = $(this).attr('imgfile');
      var todelete = $(this).attr('todelete');
      //alert(pid);
      var x = confirm("Are you sure you want to delete?");
      if(x){
      $(".modal-body2").block({ 
        message: 'Please wait, processing ...', 
        overlayCSS: { backgroundColor: '#00f' } ,
        css: { border: '3px solid #a00' } 
      }); 
      
      $.blockUI({ 
        message: 'Deleting image from product, please wait ...', 
        overlayCSS: { backgroundColor: '#00f' } 
      }); 
      //var url = $(this).attr('imgfile');

      $.ajax({
        type:"POST",
          url: "removegalleryimage.php",
          data: "imgfile="+filename+"&pid="+pid,
          dataType:"json",
          cache:false,
          success:function(rt){
            alert(rt.status);
            $.unblockUI();
            $(".modal-body2").unblock();
            $("#"+todelete).remove();
          }
      });
      e.handled = true;
    }else{
      return false;
    }

    }else{
      //alert('already clicked');
    }
  });

$(".custom-file-upload").on('click',function(e){
           $(this).find(".addImage").trigger('click');
        });
  
  $(document).on('change','.addImage',function(e){
      var pid = $(this).attr('pid');
      var sku = $(this).attr('sku');
           var file_data = $(this).prop('files')[0]; 
           var form_data = new FormData();                  
           form_data.append('file', file_data);
          $(".modal-body2").block({ 
        message: 'Please wait, processing ...', 
        overlayCSS: { backgroundColor: '#00f' } ,
        css: { border: '3px solid #a00' } 
      }); 
      
      $.blockUI({ 
        message: 'Adding new image, please wait ...', 
        overlayCSS: { backgroundColor: '#00f' } 
      }); 
           $.ajax({
            url: "addimagetogallery.php?pid="+pid+"&sku="+sku,
            type: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
               $.unblockUI();
         $(".modal-body2").unblock();
         if(data !== "Please try again later there is some error while uploading!"){
           alert("New image successfully added!");
           $("#galleryimages_"+pid).html(data);
           $(".loadcategoryitems").click();
               }else{
                alert(data);
               }
           }
        });
          
        });
/*FOR ADDING NEW TAG TO PRODUCT*/
 $("#addtag").validator().on('submit',function(event){

               if (event.isDefaultPrevented()) {
                // handle the invalid form...
                return false;
              } else {
                // everything looks good!
                event.preventDefault();
                  var session = $("#usersession").val();
                      if(session != null || session != ''){
                      var formData = $("#addnewtag").serializeArray();
                      
                      $.ajax({
                          url: 'addtags.php',
                          type: "POST",
                          data:formData,
                          success: function (rt) {
                           if(rt == "Tag successfully inserted!"){
                            $(".alert-success").css('display','block');
                            $(".notification").html("<strong>Success!</strong>"+rt);
                            $('#addtag').modal('toggle');

                            alert("Tag added successfully.");
                          }else{
                            alert(" error in performing action.");
                           $(".alert-danger").css('display','block');
                           $(".notification").html("<strong>Error!</strong>"+rt);
                           $('#addtag').modal('toggle');
                           setTimeout(function() { 
                            $(".alert-danger").css('display','none');
                             }, 4000);
                          }
                       }
                   });
                }else{
                  window.location.href = "http://173.249.20.160/magento/editcategory/admin/login.php";
                }
              }
           
            });



/*FOR ADDING TAG END HERE*/

/*FOR ADDING NEW TAG TO PRODUCT*/
 $("#addproducttag").validator().on('submit',function(event){

               if (event.isDefaultPrevented()) {
                // handle the invalid form...
                return false;
              } else {
                // everything looks good!
                event.preventDefault();
                  var session = $("#usersession").val();
                      if(session != null || session != ''){
                      var formData = $("#addproducttag").serializeArray();
                      var page = $("#page").val();
                      
                      $.ajax({
                          url: 'addproducttags.php',
                          type: "POST",
                          data:formData,
                          success: function (rt) {

                           if(rt == "Tag successfully inserted!"){
                            $(".alert-success").css('display','block');
                            $(".notification").html("<strong>Success!</strong>"+rt);
                            $('#addtag').modal('toggle');
                            if(page == "tag"){

                              window.location.reload();
                            }else{
                            $("#searchbtn").click();
                          }
                               alert("Tag added successfully.");

                          }else{
                            alert("Error.");
                           $(".alert-danger").css('display','block');
                           $(".notification").html("<strong>Error!</strong>"+rt);
                           $('#addtag').modal('toggle');
                           setTimeout(function() { 
                            $(".alert-danger").css('display','none');
                             }, 4000);
                          }
                       }
                   });
                }else{
                  
                }
              }
           
            });

/*==================== Pagnination =========================*/

$(document).on('click','.pageno',function(e){  

    var categoryid       = $(this).attr('categoryid');
    var currentpage      = $(this).attr('pageno');
    var tag              = $(this).attr('tags');
    var select_value     = $(this).attr('select_value');
    var set_price_check  = $(this).attr('set_price_check');
    var items_per_page   = $(this).attr('items_per_page');
    var product_brand    = $(this).attr('product_brand');
        
    $(this).css('background-color', 'green');
      
      $.blockUI({ 
        message: 'Please wait, loading products for page no '+currentpage+' ', 
        overlayCSS: { backgroundColor: '#00f' } 
      });       
      var pagename=window.location.href;
      $.ajax({
          method:"post",
          url: "search_form.php",
          data: "tag="+tag+"&search_value="+categoryid+"&currentpage="+currentpage+"&select_value="+select_value+"&cat="+categoryid+"&pagename="+pagename+"&set_price_check="+set_price_check+"&items_per_page="+items_per_page+"&product_brand="+product_brand,
          dataType:"json",
          cache:false,
          success:function(rt){
            //alert(rt.status);

            $("#change-content").html(rt.items);
            //$(".pagination").append("Page no: "+currentpage);
            $("[pageno="+currentpage+"]").css('background','#ccc');
          //alert(pany.items);
          $.unblockUI();
          
          if(currentpage > 6){
             
             var prev = parseInt(currentpage) -1;
             var next = parseInt(currentpage) +1;
             var nextPage =  $("#page_"+next).attr("pageno_");

             if( nextPage == undefined || nextPage == "undefined"){


             }else{
              if (parseInt(nextPage) - parseInt(currentpage) != 0 && prev > 6){

               $("#page_"+prev).attr("style","display: none");
               $("#page_"+next).removeAttr("style");
               $("#page_"+next).after('<li class="disabled" id="dot"><span style="height: 28px;line-height: 0;">...</span></li>');

             } 
             }
             if(prev == 6){

                $("#dot").attr("style","display:none");
                $("#page_"+currentpage).after('<li class="disabled" id="dot"><span style="height: 28px;line-height: 0;">...</span></li>');
             }

            

             $("#page_"+currentpage).removeAttr("style");
             $("#page_"+currentpage).attr("style","background: rgb(204, 204, 204);");
            
          }
            
          }
      });
  });

//view all category items
  $(document).on("click","#view_all_items",function(){
       var categoryid = $(this).attr('categoryid');
    var tag = $(this).attr('tags');
        var select_value = $(this).attr('select_value');
        var set_price_check = $(this).attr('set_price_check');
        var items_per_page = $(this).attr('items_per_page');
        var product_brand = $(this).attr('product_brand');
        var cat = "all";
      
      $.blockUI({ 
        message: 'Please wait, loading all products ...', 
        overlayCSS: { backgroundColor: '#00f' } 
      }); 
      
      //$(":checkbox").attr("checked", false);
     
      $(".hideme").hide();
      var pagename= window.location.href;
      $.ajax({
        method:"post",
        url:"search_form.php",
        data: "tag="+tag+"&search_value="+categoryid+"&cat="+cat+"&select_value="+select_value+"&pagename="+pagename+"&set_price_check="+set_price_check+"&items_per_page="+items_per_page+"&product_brand="+product_brand,
        dataType:"json",
        cache:false,
        success:function(pany){
          $("#change-content").html(pany.items);
          //alert(pany.items);
          $.unblockUI();
          moveModalUp();
        }
        
      });
  });

/*==================== Pagnination End=========================*/

/*==================== Featured Products start from here =========================*/


$(document).on("click","#featured",function(){
 var clcik = $(this).attr('data-id');

 if(clcik == "0" || clcik ==0){

    $(".featuredbutton").show();
  $(".addfeatureproducts").show();
$("#change-content>.improduct").each(function(){
        var itid = $(this).attr('data-id');
        $(this).children('div.thumbnail').each(function(i){
                   // $(this).children(".checkbox"); 
          $(this).prepend("<input type='checkbox' class='itemtomoveOrCopy' attr-id='"+itid+"'>");
        });
      });
var d = document.getElementById("featured");  //   Javascript
d.setAttribute('data-id' , '1'); 

} else if(clcik == "1" || clcik ==1){
$(".featuredbutton").hide();
  $(".addfeatureproducts").hide();
  $(".itemtomoveOrCopy").hide();
var d = document.getElementById("featured");  //   Javascript
d.setAttribute('data-id' , '0'); 

}

});

/*=======================================================================*/

$(document).on("click",".addfeatureproducts",function(){

    var categoryid = "1708";
    var type = $(this).attr("name");
    
    //Get for main div first
    var items = [];
    var arrayIndex=0;
    $("#change-content>.improduct input.itemtomoveOrCopy").each(function(){
      if($(this).prop('checked')){
        items[arrayIndex++] = $(this).attr('attr-id');
      }
    });
    if(items == ""){
      alert("Select and check atleast one product to "+type);
      return false;
    }
    //fire ajax to copy product item to another category
   // alert(items);
    
    //Get for Sidebox div second 
   
    // alert(innerlist);
    // var copytocat = 433;
    
    var alltogether = items;
    //alert(alltogether);
    
    $.blockUI({ 
        message: 'Please wait... '+type+' '+items.length+' products ', 
        overlayCSS: { backgroundColor: '#00f' } 
    }); 
    $.ajax({
      method: "post",
      url: "enable_product.php",
      data: "ids="+alltogether+"&type="+type,
      dataType:"json",
      cache:false,
      success:function(rt){
        alert(rt.msg);
        $.unblockUI();
        $(".featuredbutton").hide();
        $("#searchbtn").click();
        
      }
    });
  });

/*========================== single enable or disable ============================*/

$(document).on("click",".addsfeatureproducts",function(){

    var categoryid = "1708";
    var type = $(this).attr("name");
    
    //Get for main div first
    var items = $(this).attr('data-id');
    var tag = $(this).attr('page');
    
    //fire ajax to copy product item to another category
   // alert(items);
    
    //Get for Sidebox div second 
   
    // alert(innerlist);
    // var copytocat = 433;
    
    var alltogether = items;
    //alert(alltogether);
    
    $.blockUI({ 
        message: 'Please wait... '+type+' product ', 
        overlayCSS: { backgroundColor: '#00f' } 
    }); 
    $.ajax({
      method: "post",
      url: "enable_single_product.php",
      data: "ids="+alltogether+"&type="+type,
      dataType:"json",
      cache:false,
      success:function(rt){

        alert(rt.msg);
        $.unblockUI();
       
        if(tag == "tag"){

          window.location.reload();

        } else {
        $("#searchbtn").click();
        }
      
      }
    });
  });

/*==================== Featured Products start End here  =========================*/






/*checking input filed for numbers and text */

/*$(document).on("change","#select-items",function(event){
  $('#search').val('');
     var option =  $('#select-items').val();
      if(option == "item_no"){

       $('#search').keypress(function (event) {
        var keycode = event.which;
    if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
        event.preventDefault();
        return false;
    }
});

      }else{

          $( "#search" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                        return false;
                    }
                });

      }
});
*/


/*ending input field checking*/


  /*function getproductpositions(){
    var position = [];
    var arrayIndex=0;
    //$("#simpleList >li").each(function(e){
      //position[arrayIndex++] = $(this).attr('id');
    //});
    $("#simpleList>.improduct").each(function(){
      position[arrayIndex++] = $(this).attr('id');
    });
    return position;
  }

$(document).on("click",".savepositions",function(){
    var positions = getproductpositions();
    var forCategory = $("#categoryselect").val();
    if(forCategory == "" || forCategory === undefined){
      alert("Load category product and re-order product position first");
      return false;
    }
    if(positions == ""){
      alert("No product to reposition. Please load category product and try again.");
      return false;
    }
    
    $.blockUI({ 
        message: 'Please wait... Re-Ordering '+positions.length+' product positions', 
        overlayCSS: { backgroundColor: '#00f' } 
    }); 
    $.ajax({
        method:"post",
        url:"savepositions.php",
        data: "savepositions="+positions+"&cat="+$("#categoryselect").val(),
        dataType:"json",
        cache:false,
        success:function(pany){
          //$("#simpleList").html(pany.msg);
          alert(pany.msg);
          $.unblockUI();
          $(".loadcategoryitems").click();
        }
      });
  });
  
    // Simple list
  if (window.location.pathname == "/magento/editcategory/admin/index.php"){
    Sortable.create(simpleList, {
    animation: 150,
    group: { name: "product", pull:true, put:true },
  
    onUpdate: function (evt){
      //alert(evt);
      var positions = getproductpositions();
      //alert(JSON.stringify(positions));
      //alert('did you get positions?');
    },
    
    onAdd: function(evt){
      //alert('added');
      //print product position
      var positions = getproductpositions();
      //alert(JSON.stringify(positions));
      //alert('did you get positions?');
      //evt.item
    }
   }
  );

  Sortable.create(groupb, { 
    group: { name: "product", pull:true, put:true },
    //onUpdate: function (evt){
      //alert(evt);
    //}
   }
  );
}*/

/*$("#gridbtn").on("click",function(){
    if (window.matchMedia('(max-width: 767px)').matches)
    {
      $(".changeView").css('width', '143px');
      $(".thumbnail").css('height', '250px');
      $(".btn-warning").css('margin-top', '14px');
      $(".btn-success").css('margin-left', '15px');

      $(".changeView").attr('class', "col-sm-4 col-md-2 improduct changeView");
      $(".btnclose").attr("class","col-md-4");
      $(".tagbtn").hide();
      $(".btnbrand").attr("class","col-md-4");
      $(".textright").show();
      $(".row.bradname").css('margin-left', '0px');
    }
    else
    {
      $(".changeView").css('width', '16.66666667%');
      $(".changeView").attr('class', "col-sm-4 col-md-2 improduct changeView");
      $(".btnclose").attr("class","col-md-4");
      $(".tagbtn").hide();
      $(".btnbrand").attr("class","col-md-4");
      $(".textright").show();
      $(".row.bradname").css('margin-left', '0px');

      $(".thumbnail").css('height', '330px');
      $(".btn-success").css('margin-left', '14px');
      $(".btn-warning").css('margin-left', '-2px');
      $(".btn-warning").css('margin-top', '10px');
    }
});*/

// var container = document.getElementById("btnContainer");
// var btns = container.getElementsByClassName("btn");
// for (var i = 0; i < btns.length; i++) {
//   btns[i].addEventListener("click", function(){
//     var current = document.getElementsByClassName("active");
//     current[0].className = current[0].className.replace(" active", "");
//     this.className += " active";
//   });
// }
});
