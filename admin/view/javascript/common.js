function getURLVar(key) {
	var value = [];

	var query = String(document.location).split('?');

	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');

			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}

		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
}

$(document).ready(function() {

/*	$(".vakata-context li").click(function(){
		alert("cn abclab");
		var anchor_val = $("li a").attr("rel");
		alert(anchor_val);
	});*/

	//Form Submit for IE Browser
	$('button[type=\'submit\']').on('click', function() {
		$("form[id*='form-']").submit();
	});

	// Highlight any found errors
	$('.text-danger').each(function() {
		var element = $(this).parent().parent();

		if (element.hasClass('form-group')) {
			element.addClass('has-error');
		}
	});

	// tooltips on hover
	$('[data-toggle=\'tooltip\']').tooltip({container: 'body', html: true});

	// Makes tooltips work on ajax generated content
	$(document).ajaxStop(function() {
		$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
	});

	// https://github.com/opencart/opencart/issues/2595
	$.event.special.remove = {
		remove: function(o) {
			if (o.handler) {
				o.handler.apply(this, arguments);
			}
		}
	}
	
	// tooltip remove
	$('[data-toggle=\'tooltip\']').on('remove', function() {
		$(this).tooltip('destroy');
	});

	// Tooltip remove fixed
	$(document).on('click', '[data-toggle=\'tooltip\']', function(e) {
		$('body > .tooltip').remove();
	});
	
	$('#button-menu').on('click', function(e) {
		e.preventDefault();
		
		$('#column-left').toggleClass('active');
	});

	// Set last page opened on the menu
	$('#menu a[href]').on('click', function() {
		sessionStorage.setItem('menu', $(this).attr('href'));
	});

	if (!sessionStorage.getItem('menu')) {
		$('#menu #dashboard').addClass('active');
	} else {
		// Sets active and open to selected page in the left column menu.
		$('#menu a[href=\'' + sessionStorage.getItem('menu') + '\']').parent().addClass('active');
	}
	
	$('#menu a[href=\'' + sessionStorage.getItem('menu') + '\']').parents('li > a').removeClass('collapsed');
	
	$('#menu a[href=\'' + sessionStorage.getItem('menu') + '\']').parents('ul').addClass('in');
	
	$('#menu a[href=\'' + sessionStorage.getItem('menu') + '\']').parents('li').addClass('active');
	
	// Image Manager
	$(document).on('click', 'a[data-toggle=\'image\']', function(e) {
		var $element = $(this);
		var $popover = $element.data('bs.popover'); // element has bs popover?

		e.preventDefault();

		// destroy all image popovers
		$('a[data-toggle="image"]').popover('destroy');

		// remove flickering (do not re-add popover when clicking for removal)
		if ($popover) {
			return;
		}

		$element.popover({
			html: true,
			placement: 'right',
			trigger: 'manual',
			content: function() {
				return '<button type="button" id="button-image" class="btn btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
			}
		});

		$element.popover('show');

		$('#button-image').on('click', function() {
			var $button = $(this);
			var $icon   = $button.find('> i');

			$('#modal-image').remove();

			$.ajax({
				url: 'index.php?route=common/filemanager&user_token=' + getURLVar('user_token') + '&target=' + $element.parent().find('input').attr('id') + '&thumb=' + $element.attr('id'),
				dataType: 'html',
				beforeSend: function() {
					$button.prop('disabled', true);
					if ($icon.length) {
						$icon.attr('class', 'fa fa-circle-o-notch fa-spin');
					}
				},
				complete: function() {
					$button.prop('disabled', false);

					if ($icon.length) {
						$icon.attr('class', 'fa fa-pencil');
					}
				},
				success: function(html) {
					$('body').append('<div id="modal-image" class="modal">' + html + '</div>');

					$('#modal-image').modal('show');
				}
			});

			$element.popover('destroy');
		});

		$('#button-clear').on('click', function() {
			$element.find('img').attr('src', $element.find('img').attr('data-placeholder'));

			$element.parent().find('input').val('');

			$element.popover('destroy');
		});
	});

	$("#jstree").jstree({
			"core" : {
	      "check_callback" : true
	    },
		"plugins": ["contextmenu"],
		'contextmenu' : {
        'items' : customMenu
    },
	});

	function customMenu(node)
	{
	    var items = {
	        'createItem' : {
	            'label' : 'Create',
	            'accesskey': "e",
	            'icon': "create",
	            'action': function() {
		            var cat_id = $(node).attr('id');
		            var cat_name = $(node).attr('text');
		            cat_name = cat_name.replace(/<[^>]*>?/gm, '');
		            $(".modal-title").html("New Category");
		            $(".modal-item-container").html("<div class='form-group'><label for='name'>Category Name:</label><input class='newcat form-control' type='text' name='' id='"+cat_id+"' value=''/></div><button class='btn btn-success btn-newcat'>Submit</button>");
		            $("#myModal").modal('show');
		          }
	        },
	        'renameItem' : {
	            'label' : 'Rename',
	            'action' : function () {
	            	// $(".childopt").remove('b');
	            	var cat_id = $(node).attr('id');
		            var cat_name = $(node).attr('text');
		            cat_name = cat_name.replace(/<[^>]*>?/gm, '');
		            if(cat_id == "j1_1"){
		            	return false;
		            }
		            $(".modal-title").html("Rename Category");
		            $(".modal-item-container").html("<div class='form-group'><input class='categoryEdit form-control' type='text' name='' id='"+cat_id+"' value='"+cat_name+"'/></div><button class='btn btn-success btn-update-catName'>Update</button>");
		            $("#myModal").modal('show');
	            }
	        },
	        'deleteItem' : {
	            'label' : 'Delete',
	            'action' : function () { 
	            	var cat_id = $(node).attr('id');
		            var cat_name = $(node).attr('text');
		            cat_name = cat_name.replace(/<[^>]*>?/gm, '');
		            if(cat_id == "j1_1"){
		            	return false;
		            }
		            $(".modal-title").html("Delete Category");
		            $(".modal-item-container").html("<label for='sel1'>Confirm category delete</label>&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-success btn-cat-delete btn-sm'>Submit Confirmation</button><input class='categorydelete' type='hidden' name='' id='"+cat_id+"'/>");
		            $("#myModal").modal('show');
	            }
	        },
	        'copyItem' : {
	            'label' : 'Copy',
	            'action' : function () { 
	            	var cat_id = $(node).attr('id');
		            var cat_name = $(node).attr('text');
		            cat_name = cat_name.replace(/<[^>]*>?/gm, '');
		            if(cat_id == "j1_1"){
		            	return false;
		            }
		            $(".modal-title").html("Copy Category");
		            $(".modal-item-container").html("<div class='form-group'><label for='name'>Category Name:</label><input class='copycat form-control' type='text' name='"+cat_name+"' id='"+cat_id+"' value=''/></div><button class='btn btn-success btn-newcat'>Submit</button>");
		            // $("#myModal").modal('show');
	            }
	        },
	        'cutItem' : {
	            'label' : 'Cut',
	            'action' : function () {
	            	var cat_id = $(node).attr('id');
		            var cat_name = $(node).attr('text');
		            cat_name = cat_name.replace(/<[^>]*>?/gm, '');
		            if(cat_id == "j1_1"){
		            	return false;
		            }
		            $(".modal-title").html("Copy Category");
		            $(".modal-item-container").html("<div class='form-group'><label for='name'>Category Name:</label><input class='cutcat form-control' type='text' name='"+cat_name+"' id='"+cat_id+"' value=''/></div><button class='btn btn-success btn-newcat'>Submit</button>");
		            // $("#myModal").modal('show');
	            }
	        },
	        'pasteItem' : {
	            'label' : 'Paste',
	            'action' : function () {
	            	var hash_value = window.location.href.substr(1).split('=');
				    var user_token = hash_value[2];
	            	var copy_cat_id = $(".copycat").attr('id');
	            	var copy_cat_name = $(".copycat").attr('name');
	            	var cut_cat_id = $(".cutcat").attr('id');
	            	var cut_cat_name = $(".cutcat").attr('name');
	            	var paste_cat_id = $(node).attr('id');
		            var cat_name = $(node).attr('text');
		            cat_name = cat_name.replace(/<[^>]*>?/gm, '');
		            if(paste_cat_id == "j1_1"){
		            	return false;
		            }
		            if(copy_cat_id == undefined && copy_cat_name == undefined){
		            	copy_cat_id = "";
		            	copy_cat_name = "";
		            }

		            if(cut_cat_id == undefined && cut_cat_name == undefined){
		            	cut_cat_id = "";
		            	cut_cat_name = "";
		            }

	            	// alert(copy_cat_id+ " " + copy_cat_name + " " + paste_cat_id); return false;
		            $.ajax({
						method: "post",
						url: "index.php?route=sorting/category_sorting/add&user_token="+user_token,
						data: "createcategory=1&parent="+paste_cat_id+"&name="+copy_cat_name+"&catId="+copy_cat_id+"&cutCatId="+cut_cat_id+"&cutCatName="+cut_cat_name,
						dataType:"json",
						cache:false,
						complete:function(rt){
							if(rt.msg == undefined){
								alert("Category Updated Successfully!");
							}
							
					        $("#myModal").hide();
					        $(".modal-backdrop").remove();
					        location.reload();
						}
					// return false;
					});
	            }
	        },
	        'enableDisable' : {
	            'label' : 'Enable/Disable',
	            'accesskey': "e",
	            'icon': "enableDisable",
	            'action' : function () {
	            	var cat_id = $(node).attr('id');
		            var cat_name = $(node).attr('text');
		            cat_name = cat_name.replace(/<[^>]*>?/gm, '');
		            if(cat_id == "j1_1"){
		            	return false;
		            }
		            $(".modal-title").html("Enable/Disable Category");
		            $(".modal-item-container").html("<div class='form-group'><label for='sel1'>Select a Category Status:</label><select class='input-small form-control categorystatus'><option value='2'>Select Status</option><option value='1'>Activate</option><option value='0'>De-Activate</option></select></div><button class='btn btn-success btn-cat-status-update btn-sm'>Update</button><input class='categorystatusupdate' type='hidden' name='' id='"+cat_id+"'/>");
	            	$("#myModal").modal('show');
	            }
	        },
	        'store' : {
	            'label' : 'Visit Store',
	            'accesskey': "e",
	            'icon': "Visit Store",
	            'action' : function () {
	            	var cat_id = $(node).attr('id');
	            	if(cat_id == "j1_1"){
		            	return false;
		            }
		            window.open('http://134.209.72.8/index.php?route=product/category&path='+cat_id, '_blank');
	            }
	        },
	        'sortingStore' : {
	            'label' : 'Visit Sorting Store',
	            'accesskey': "e",
	            'icon': "Visit Sorting Store",
	            'action' : function () {
	            	var hash_value = window.location.href.substr(1).split('=');
				    var user_token = hash_value[2];
	            	var cat_id = $(node).attr('id');
	            	if(cat_id == "j1_1"){
		            	return false;
		            }
		            window.open('http://134.209.72.8/admin/index.php?route=sorting/category_sorting&user_token='+user_token, '_blank');
	            }
	        }
	    }
	    return items;
	}

	var hash_value = window.location.href.substr(1).split('=');
    var user_token = hash_value[2];

  	$(document).on("click",".btn-newcat",function(e){
		var newCatname = $(".newcat").val();
		var parentcatId = $(".newcat").attr('id');
		var catId = "";
		var cut_cat_id = "";
    	var cut_cat_name = "";
		if(parentcatId == "j1_1"){
			parentcatId = 0;
		}

		$.ajax({
			method: "post",
			url: "index.php?route=sorting/category_sorting/add&user_token="+user_token,
			data: "createcategory=1&parent="+parentcatId+"&name="+newCatname+"&catId="+catId+"&cutCatId="+cut_cat_id+"&cutCatName="+cut_cat_name,
			dataType:"json",
			cache:false,
			success:function(rt){
				alert(rt.msg);
		        $("#myModal").hide();
		        $(".modal-backdrop").remove();
        // $(".modal-open").css("overflow","scroll");
		        location.reload();
			}
		});
	});

	$(document).on("click",".btn-update-catName",function(){

		var newCatname = $(".categoryEdit").val();
		var catId = $(".categoryEdit").attr('id');
		var status = "";
		if(catId == "j1_1"){
			catId = 0;
		}
		$.ajax({
			method: "post",
			url: "index.php?route=sorting/category_sorting/edit&user_token="+user_token,
			data: "catid="+catId+"&name="+newCatname+"&status="+status,
			dataType:"json",
			cache:false,
			success:function(rt){
	        $("#myModal").hide();
	        $(".modal-backdrop").remove();
	        $(".modal-open").css("overflow","scroll");
				alert(rt.msg);
				
				if($("#410_anchor>span.editable").find("b").length>0){
				 $("#"+catId+"_anchor>span.editable").html("<b>"+newCatname+"</b>");
				}else{
				$("#"+catId+"_anchor>span.editable").html(newCatname);
				}
        location.reload();
				//$.unblockUI();
			}
		});
	});

	$(document).on("click",".btn-cat-delete",function(){
		var catId = $(".categorydelete").attr('id');
		$.ajax({
			method: "post",
			url: "index.php?route=sorting/category_sorting/delete&user_token="+user_token,
			data: "catid="+catId,
			dataType:"json",
			cache:false,
			success:function(rt){
	        $("#myModal").hide();
	        $(".modal-backdrop").remove();
	        $(".modal-open").css("overflow","scroll");
					alert(rt.msg);
	        location.reload();
			}
		});
	});

	$(document).on("click",".btn-cat-status-update",function(){
		var status = $(".categorystatus").val();
		var catId = $(".categorystatusupdate").attr('id');
		var name = "";
		if(status==2){
			alert('Please choose Category Status');
			return false;
		}
		$.ajax({
			method: "post",
			url: "index.php?route=sorting/category_sorting/edit&user_token="+user_token,
			data: "catid="+catId+"&status="+status+"&name="+name,
			dataType:"json",
			cache:false,
			success:function(rt){
        $("#myModal").hide();
        $(".modal-backdrop").remove();
        $(".modal-open").css("overflow","scroll");
				alert(rt.msg);
        location.reload();
				//$.unblockUI();
			}
		});
	});

	$('#collapse-all').click(function () {
	  $('#jstree').jstree('close_all');
	});
	$('#expand-all').click(function () {
	  $('#jstree').jstree('open_all');
	});

});

// Autocomplete */
(function($) {
	$.fn.autocomplete = function(option) {
		return this.each(function() {
			var $this = $(this);
			var $dropdown = $('<ul class="dropdown-menu" />');

			this.timer = null;
			this.items = [];

			$.extend(this, option);

			$this.attr('autocomplete', 'off');

			// Focus
			$this.on('focus', function() {
				this.request();
			});

			// Blur
			$this.on('blur', function() {
				setTimeout(function(object) {
					object.hide();
				}, 200, this);
			});

			// Keydown
			$this.on('keydown', function(event) {
				switch(event.keyCode) {
					case 27: // escape
						this.hide();
						break;
					default:
						this.request();
						break;
				}
			});

			// Click
			this.click = function(event) {
				event.preventDefault();

				var value = $(event.target).parent().attr('data-value');

				if (value && this.items[value]) {
					this.select(this.items[value]);
				}
			}

			// Show
			this.show = function() {
				var pos = $this.position();

				$dropdown.css({
					top: pos.top + $this.outerHeight(),
					left: pos.left
				});

				$dropdown.show();
			}

			// Hide
			this.hide = function() {
				$dropdown.hide();
			}

			// Request
			this.request = function() {
				clearTimeout(this.timer);

				this.timer = setTimeout(function(object) {
					object.source($(object).val(), $.proxy(object.response, object));
				}, 200, this);
			}

			// Response
			this.response = function(json) {
				var html = '';
				var category = {};
				var name;
				var i = 0, j = 0;

				if (json.length) {
					for (i = 0; i < json.length; i++) {
						// update element items
						this.items[json[i]['value']] = json[i];

						if (!json[i]['category']) {
							// ungrouped items
							html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
						} else {
							// grouped items
							name = json[i]['category'];
							if (!category[name]) {
								category[name] = [];
							}

							category[name].push(json[i]);
						}
					}

					for (name in category) {
						html += '<li class="dropdown-header">' + name + '</li>';

						for (j = 0; j < category[name].length; j++) {
							html += '<li data-value="' + category[name][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[name][j]['label'] + '</a></li>';
						}
					}
				}

				if (html) {
					this.show();
				} else {
					this.hide();
				}

				$dropdown.html(html);
			}

			$dropdown.on('click', '> li > a', $.proxy(this.click, this));
			$this.after($dropdown);
		});
	}
})(window.jQuery);
