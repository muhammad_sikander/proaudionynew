<?php
$_['text_module'] = 'Modules';
$_['cartName'] = 'OpenCart';
$_['sourceCartName'] = 'Magento';
$_['sourceCartNameLink'] = 'magento';
$_['sourceCartNameImg'] = 'magento';
$_['heading_title'] = 'Cart2cart: Magento to OpenCart Migration Module';
$_['referer_text'] = 'Cart2cart: Magento to OpenCart Migration Module';

$_['app_url'] = 'https://opencart.app.shopping-cart-migration.com/';
$_['module_url'] = $_['app_url'] . 'from-magento-to-opencart/';

$_['banner'] = '<div class="banner" onclick="javascript: window.open(\'https://www.shopping-cart-migration.com/provided-services/data-migration-service-packages?utm_source='. $_['cartName']  . '&utm_medium=Plugins\',\'_blank\'); return false;"><img src="view/image/cart2cart/banner.png" /></div>';
$_['cart2cart_logo'] = '<a target="_blank" class="cart2cart_logo" href="http://www.shopping-cart-migration.com/?utm_source='. $_['cartName']  . '&utm_medium=Plugins&utm_campaign=c2cOpencart"  target="_blank">';