<?php
class ControllerExtensionModuleC2c extends Controller
{
  public function index()
  {
    $this->language->load('extension/module/c2c');

    $this->document->setTitle($this->language->get('heading_title'));

    $data['src_url'] = $this->language->get('module_url') . '?storeUrl=' . HTTPS_CATALOG;

    $data['breadcrumbs'] = array(
      array(
        'text'      => $this->language->get('text_home'),
        'href'      => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], 'SSL'),
        'separator' => ' :: '
      ),
      array(
        'text'      => $this->language->get('text_module'),
        'href'      => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], 'SSL'),
        'separator' => ' :: '
      ),
      array(
        'text'      => $this->language->get('heading_title'),
        'href'      => $this->url->link('extension/module/c2c', 'user_token=' . $this->session->data['user_token'], 'SSL'),
        'separator' => ' :: '
      )
    );

    if (property_exists($this,'data')) {
      $this->template = 'extension/module/c2c';
      $this->children = array(
        'common/header',
        'common/footer'
      );

      $data['version'] = 1;
      $this->data = $data;

      $this->response->setOutput($this->render());
    } else {
      $data['header'] = $this->load->controller('common/header');
      $data['column_left'] = $this->load->controller('common/column_left');
      $data['footer'] = $this->load->controller('common/footer');

      $data['version'] = 2;

      $this->response->setOutput($this->load->view('extension/module/c2c', $data));
    }
  }
}