<?php
class ControllerSortingCategorySorting extends Controller {
	private $error = array();
	public function index() {

		$this->load->language('sorting/category_sorting');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->getList();
	}

	public function add() {
		$this->load->language('sorting/category_sorting');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$catname  = $_POST['name'];
			$parentId = $_POST['parent'];
			$catId = $_POST['catId'];
			$cut_cat_id = $_POST['cutCatId'];
			$cut_cat_name = $_POST['cutCatName'];

// print_r("name ". $catname. " parent ". $parentId. " catid ". $catId. " cutCatId ". $cut_cat_id. " cutCatName ". $cut_cat_name); exit();

			if($cut_cat_name != ""){
				$catname  = $_POST['cutCatName'];
			}else{
				$catname  = $catname;
			}

			if($cut_cat_id != ""){
				$catId = $cut_cat_id;
			}else{
				$catId = $catId;
			}

			$category_total = $this->model_catalog_category->getCategory($parentId);
			if(!empty($category_total)){
				if(empty($category_total['path'])){
					$path = $category_total['name'];
				}else{
					$path = $category_total['path'].'&nbsp;&nbsp;&gt;&nbsp;&nbsp;'.$category_total['name'];
				}
			}else{
				$path = '--- None ---';
			}

			if($catname == ""){
				echo json_encode(array('msg' => "Specify a name for your new category") );
				exit();
			}
			
			$msg = '';
			/*$msgcc = '';
			$msgcm = '';*/
			$data = array();
			try{
				$data = array(
					'category_description' =>array(
						'1'=>array(
							'name' => $catname,
							'description' => '',
							'meta_title' => $catname,
							'meta_description' => '',
							'meta_keyword' => ''
						)
					),
					'path' => $path,
					'parent_id' => $parentId,
					'filter' => '',
					'category_store' => array(
						'0' => '0'
					),
					'image' => '',
					'column' => 1,
					'sort_order' => 0,
					'status' => 1,
					'category_seo_url' => array(
						'0' => array(
							'1' => ''
						)
					),
					'category_layout' => array(
						'0' => ''
					)
				);

				$this->model_catalog_category->addCategory($data);

				// sleep(2);

			if($catId != ""){
				$latest_cat = $this->model_catalog_product->getLatestCategoryId();
				echo "<pre>";
				$latest_cat = $latest_cat['max(category_id)'];
				$copy_cat_product = "";
				$copy_cat_product = $this->model_catalog_product->getProductsByCategoryId($catId);
				// print_r($copy_cat_product); exit();
				echo "<pre>";
				$product_category_id = array();
				if(count($copy_cat_product) > 0){
					foreach ($copy_cat_product as $copy_product) {
						$product_category_id[] = $copy_product['category_id'];
						$new_parent = array();
						$new_parent[] = $latest_cat;
						$new_product_cat = array_merge($product_category_id, $new_parent);
						$up_pro_category = array_unique($new_product_cat);
						$extra_product_img = array();
						if(!array_key_exists('product_image', $copy_product)){
							$extra_product_img = array();
						}else{
							$extra_product_img = $copy_product['product_image'];
						}

						$updatePro = array(
						    'model' => $copy_product['model'],
						    'sku' => $copy_product['sku'],
						    'upc' => '',
						    'ean' => '',
						    'jan' => '',
						    'isbn' => '',
						    'mpn' => '',
						    'location' => '',
						    'quantity' => $copy_product['quantity'],
						    'minimum' => '1',
						    'subtract' => '',
						    'stock_status_id' => $copy_product['stock_status_id'],
						    'date_available' => '',
						    'manufacturer_id' => $copy_product['manufacturer_id'],
						    'shipping' => '',
						    'price' => intval(preg_replace('/[^\d.]/', '', $copy_product['price'])),
						    'list_price' => intval(preg_replace('/[^\d.]/', '', $copy_product['list_price'])),
						    'points' => '',
						    'weight' => $copy_product['weight'],
						    'weight_class_id' => '',
						    'length' => $copy_product['length'],
						    'width' => $copy_product['width'],
						    'height' => $copy_product['height'],
						    'length_class_id' => '',
						    'status' => '1',
						    'tax_class_id' => '9',
						    'sort_order' => '',
						    'product_store' => array(
					    	 'product_store' => '0'
					    	),
						    'product_category' => $up_pro_category,
						    'image' => trim($copy_product['image']),
						    'product_image' => $extra_product_img,
						    'product_description' => array(
					    	1=>array('name' => $copy_product['name'],
					    	'description' => $copy_product['description'],
					    	'meta_title' => $copy_product['meta_title'],
					    	'meta_description' => '',
					    	'meta_keyword' => '',
					    	'tag' => ''
					    	),
						 ),
						);
						
						$this->model_catalog_product->editProduct($copy_product['product_id'], $updatePro);
					}
				}
				if($cut_cat_id != ""){
					$this->model_catalog_category->deleteCategory($cut_cat_id);
					$msg .='Category Moved!!!';
				}else{
					$msg .='Category Copied!!!';
				}
			 }else{
			 	$msg .='Category Created!!!';
			 }
			} catch(Exception $e) {
			    $msg .= $e;
			}
		}
			echo json_encode(array('msg' => $msg));
			exit();
	}

	public function edit() {
		$this->load->language('sorting/category_sorting');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/category');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$catId  = $_POST['catid'];
			$newCatname = $_POST['name'];
			$status = $_POST['status'];
			// print_r($catId." ".$newCatname." ".$status); exit();
			$category_total = $this->model_catalog_category->getAllCategoryData($catId);
			$get_cat_path = $this->model_catalog_category->getCategory($catId);
			if(!empty($get_cat_path)){
				if(!empty($get_cat_path['path'])){
					$cat_pth = $get_cat_path['path'].'&nbsp;&nbsp;&gt;&nbsp;&nbsp;'.$get_cat_path['name'];
				}else{
					$cat_pth = '';
				}
			}else{
				$cat_pth = '';
			}
			$category_data = $category_total[0];
			$category_data['path'] = $cat_pth;

			if($newCatname == ""){
				$new_Cat_name = $category_data['name'];
			}else{
				$new_Cat_name = $newCatname;
			}

			if($status == ""){
				$new_Cat_status = $category_data['status'];
			}else{
				$new_Cat_status = $status;
			}
			
			$msg = '';
			$data = array();
			try{
				$data = array(
					'category_description' =>array(
						'1'=>array(
							'name' => $new_Cat_name,
							'description' => $category_data['description'],
							'meta_title' => $category_data['name'],
							'meta_description' => $category_data['meta_description'],
							'meta_keyword' => $category_data['meta_keyword']
						)
					),
					'path' => $category_data['path'],
					'parent_id' => $category_data['parent_id'],
					'filter' => '',
					'category_store' => array(
						'0' => '0'
					),
					'image' => $category_data['image'],
					'column' => $category_data['column'],
					'sort_order' => $category_data['sort_order'],
					'status' => $new_Cat_status,
					'category_seo_url' => array(
						'0' => array(
							'1' => ''
						)
					),
					'category_layout' => array(
						'0' => ''
					)
				);
				// print_r($data); exit();
				$this->model_catalog_category->editCategory($catId, $data);
			    $msg .='Category Updated!!!';
			} catch(Exception $e) {
			    $msg .= $e;
			}
			echo json_encode(array('msg' => $msg));
			exit();
		}
	}

	public function delete() {
		$this->load->language('sorting/category_sorting');
		$this->load->model('catalog/category');

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$del_cat_id  = $_POST['catid'];
			$msg = '';
			try {
				if($del_cat_id != "") {
				$this->model_catalog_category->deleteCategory($del_cat_id);
				$msg .='Category Deleted!!!';
				}else{
					$msg .='Please select category to delete!!!';
				}
			} catch (Exception $e) {
				$msg .= $e;
			}
			echo json_encode(array('msg' => $msg));
			exit();
		}
	}

	public function repair() {
		$this->load->language('sorting/category_sorting');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/category');

		if ($this->validateRepair()) {
			$this->model_catalog_category->repairCategories();

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/category/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/category/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['repair'] = $this->url->link('catalog/category/repair', 'user_token=' . $this->session->data['user_token'] . $url, true);

		$data['categories'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$category_total = $this->model_catalog_category->getTotalCategories();
		$results = $this->model_catalog_category->getParentChildCategories($filter_data);
		$all_cat_results = $this->model_catalog_category->getCategoriesIdsNames();

		if(count($all_cat_results) > 0){
			$select_html ='<ul><li catname="Default Category">ProAudioNY<ul>';
			$select_html .='';
			$cat_status="";
			$total_product_category = "";
			$total_product_count = "";
			foreach ($all_cat_results as $value) {
				if($value['status'] == 0){
					$cat_status = 'disabled';
				}else{
					$cat_status = '';
				}
				if($value['parent_id'] == 0){
				/*$total_product_category = $this->model_catalog_product->getProductsByCategoryId($value['category_id']);
				$total_product_count = count($total_product_category);*/
				$select_html.= '<li catname="'.$value['name'].'" id="'.$value['category_id'].'" class="parentopt contextmenu '.$cat_status.'">'.str_replace('&amp;', '&', $value['name']);
				}
			$get_child_ids = $this->model_catalog_category->getAllCategoriesIds($value['category_id']);
			if(!empty($get_child_ids)){
				$select_html .= '<ul>';
			foreach ($get_child_ids as $sub_cat) {
				if($sub_cat['status'] == 0){
					$cat_status = 'disabled';
				}else{
					$cat_status = '';
				}

				/*$total_product_category = $this->model_catalog_product->getProductsByCategoryId($sub_cat['category_id']);
				$total_product_count = count($total_product_category);*/

				$select_html.= '<li catname="'.$sub_cat['name'].'" id="'.$sub_cat['category_id'].'" class="childopt contextmenu '.$cat_status.'" pid="'.$sub_cat['parent_id'].'"> '.$sub_cat['name'];

					$get_child_child_ids = $this->model_catalog_category->getAllCategoriesIds($sub_cat['category_id']);
					if(!empty($get_child_child_ids)){
						$select_html .= '<ul>';
						foreach ($get_child_child_ids as $sub_sub_cat) {
							if($sub_sub_cat['status'] == 0){
								$cat_status = 'disabled';
							}else{
								$cat_status = '';
							}

							$total_product_category = $this->model_catalog_product->getProductsByCategoryId($sub_sub_cat['category_id']);
							$total_product_count = count($total_product_category);

							$select_html.= '<li catname="'.$sub_sub_cat['name'].'" id="'.$sub_sub_cat['category_id'].'" pid="'.$sub_sub_cat['parent_id'].'" class="childopt contextmenu '.$cat_status.'">'.$sub_sub_cat["name"]. "<b style= color:#93d200dd> (". $total_product_count .")</b>";

							$get_3rd_child_ids = $this->model_catalog_category->getAllCategoriesIds($sub_sub_cat['category_id']);
							if(!empty($get_3rd_child_ids)){
							$select_html .= '<ul>';
							foreach ($get_3rd_child_ids as $sub_3rd_cat) {
								if($sub_3rd_cat['status'] == 0){
									$cat_status = 'disabled';
								}else{
									$cat_status = '';
								}

								$total_product_category = $this->model_catalog_product->getProductsByCategoryId($sub_3rd_cat['category_id']);
								$total_product_count = count($total_product_category);

								$select_html.= '<li catname="'.$sub_3rd_cat['name'].'" id="'.$sub_3rd_cat['category_id'].'" pid="'.$sub_3rd_cat['parent_id'].'" class="childopt contextmenu '.$cat_status.'"> '.$sub_3rd_cat['name']. "<b style= color:#93d200dd> (". $total_product_count .")</b></li>";
							}
							$select_html .= '</li></ul></li>';
						 }else{
								$select_html .= '</li></li>';
						  } 
						}
						$select_html .= '</li></ul></li>';
					}else{
						$select_html .= '</li></li>';
				  } 
				}
				$select_html .= '</ul></li>';
			}else{
				$select_html .= '</li>';
			}
		}
		$select_html.='</ul></li></ul>';
	}

		$data['select_html'] = $select_html;

		foreach ($results as $result) {
			$data['categories'][] = array(
				'category_id' => $result['category_id'],
				'name'        => $result['name'],
				'sort_order'  => $result['sort_order'],
				'edit'        => $this->url->link('catalog/category/edit', 'user_token=' . $this->session->data['user_token'] . '&category_id=' . $result['category_id'] . $url, true),
				'delete'      => $this->url->link('catalog/category/delete', 'user_token=' . $this->session->data['user_token'] . '&category_id=' . $result['category_id'] . $url, true)
			);
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
// print_r($data); exit();
		$this->response->setOutput($this->load->view('sorting/category_sorting', $data));
	}

	protected function getForm() {
		$data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		if (isset($this->error['parent'])) {
			$data['error_parent'] = $this->error['parent'];
		} else {
			$data['error_parent'] = '';
		}
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		if (!isset($this->request->get['category_id'])) {
			$data['action'] = $this->url->link('catalog/category/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/category/edit', 'user_token=' . $this->session->data['user_token'] . '&category_id=' . $this->request->get['category_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'] . $url, true);

		if (isset($this->request->get['category_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_catalog_category->getCategory($this->request->get['category_id']);
		}

		$data['user_token'] = $this->session->data['user_token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['category_description'])) {
			$data['category_description'] = $this->request->post['category_description'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_description'] = $this->model_catalog_category->getCategoryDescriptions($this->request->get['category_id']);
		} else {
			$data['category_description'] = array();
		}

		if (isset($this->request->post['path'])) {
			$data['path'] = $this->request->post['path'];
		} elseif (!empty($category_info)) {
			$data['path'] = $category_info['path'];
		} else {
			$data['path'] = '';
		}

		if (isset($this->request->post['parent_id'])) {
			$data['parent_id'] = $this->request->post['parent_id'];
		} elseif (!empty($category_info)) {
			$data['parent_id'] = $category_info['parent_id'];
		} else {
			$data['parent_id'] = 0;
		}
// print_r($data['parent_id']); exit();
		$this->load->model('catalog/filter');

		if (isset($this->request->post['category_filter'])) {
			$filters = $this->request->post['category_filter'];
		} elseif (isset($this->request->get['category_id'])) {
			$filters = $this->model_catalog_category->getCategoryFilters($this->request->get['category_id']);
		} else {
			$filters = array();
		}

		$data['category_filters'] = array();

		foreach ($filters as $filter_id) {
			$filter_info = $this->model_catalog_filter->getFilter($filter_id);

			if ($filter_info) {
				$data['category_filters'][] = array(
					'filter_id' => $filter_info['filter_id'],
					'name'      => $filter_info['group'] . ' &gt; ' . $filter_info['name']
				);
			}
		}

		$this->load->model('setting/store');

		$data['stores'] = array();
		
		$data['stores'][] = array(
			'store_id' => 0,
			'name'     => $this->language->get('text_default')
		);
		
		$stores = $this->model_setting_store->getStores();

		foreach ($stores as $store) {
			$data['stores'][] = array(
				'store_id' => $store['store_id'],
				'name'     => $store['name']
			);
		}

		if (isset($this->request->post['category_store'])) {
			$data['category_store'] = $this->request->post['category_store'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_store'] = $this->model_catalog_category->getCategoryStores($this->request->get['category_id']);
		} else {
			$data['category_store'] = array(0);
		}

		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($category_info)) {
			$data['image'] = $category_info['image'];
		} else {
			$data['image'] = '';
		}

		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($category_info) && is_file(DIR_IMAGE . $category_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($category_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		if (isset($this->request->post['top'])) {
			$data['top'] = $this->request->post['top'];
		} elseif (!empty($category_info)) {
			$data['top'] = $category_info['top'];
		} else {
			$data['top'] = 0;
		}

		if (isset($this->request->post['column'])) {
			$data['column'] = $this->request->post['column'];
		} elseif (!empty($category_info)) {
			$data['column'] = $category_info['column'];
		} else {
			$data['column'] = 1;
		}

		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($category_info)) {
			$data['sort_order'] = $category_info['sort_order'];
		} else {
			$data['sort_order'] = 0;
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($category_info)) {
			$data['status'] = $category_info['status'];
		} else {
			$data['status'] = true;
		}
		
		if (isset($this->request->post['category_seo_url'])) {
			$data['category_seo_url'] = $this->request->post['category_seo_url'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_seo_url'] = $this->model_catalog_category->getCategorySeoUrls($this->request->get['category_id']);
		} else {
			$data['category_seo_url'] = array();
		}
				
		if (isset($this->request->post['category_layout'])) {
			$data['category_layout'] = $this->request->post['category_layout'];
		} elseif (isset($this->request->get['category_id'])) {
			$data['category_layout'] = $this->model_catalog_category->getCategoryLayouts($this->request->get['category_id']);
		} else {
			$data['category_layout'] = array();
		}

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/category_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/category')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['category_description'] as $language_id => $value) {
			if ((utf8_strlen($value['name']) < 1) || (utf8_strlen($value['name']) > 255)) {
				$this->error['name'][$language_id] = $this->language->get('error_name');
			}

			if ((utf8_strlen($value['meta_title']) < 1) || (utf8_strlen($value['meta_title']) > 255)) {
				$this->error['meta_title'][$language_id] = $this->language->get('error_meta_title');
			}
		}

		if (isset($this->request->get['category_id']) && $this->request->post['parent_id']) {
			$results = $this->model_catalog_category->getCategoryPath($this->request->post['parent_id']);
			
			foreach ($results as $result) {
				if ($result['path_id'] == $this->request->get['category_id']) {
					$this->error['parent'] = $this->language->get('error_parent');
					
					break;
				}
			}
		}

		if ($this->request->post['category_seo_url']) {
			$this->load->model('design/seo_url');
			
			foreach ($this->request->post['category_seo_url'] as $store_id => $language) {
				foreach ($language as $language_id => $keyword) {
					if (!empty($keyword)) {
						if (count(array_keys($language, $keyword)) > 1) {
							$this->error['keyword'][$store_id][$language_id] = $this->language->get('error_unique');
						}

						$seo_urls = $this->model_design_seo_url->getSeoUrlsByKeyword($keyword);
	
						foreach ($seo_urls as $seo_url) {
							if (($seo_url['store_id'] == $store_id) && (!isset($this->request->get['category_id']) || ($seo_url['query'] != 'category_id=' . $this->request->get['category_id']))) {		
								$this->error['keyword'][$store_id][$language_id] = $this->language->get('error_keyword');
				
								break;
							}
						}
					}
				}
			}
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/category')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateRepair() {
		if (!$this->user->hasPermission('modify', 'catalog/category')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/category');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_catalog_category->getCategories($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'category_id' => $result['category_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
