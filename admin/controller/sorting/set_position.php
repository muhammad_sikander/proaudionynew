<?php
class ControllerSortingSetPosition extends Controller {
	private $error = array();
	public function index()
	{
		$this->load->language('sorting/set_position');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->setProductPosition();

	}

	public function setProductPosition(){
		$url = "";
		$breadcrumbs_path = 'sorting/set_position';
		$load_cat_products = $this->load->controller('sorting/set_image/getCategoryList', $breadcrumbs_path);

		return $load_cat_products;
	}

	public function updateProductPosition(){
		$this->load->model('catalog/product');
		if(($this->request->server['REQUEST_METHOD'] == 'POST')){
			$msg = "";
			$sorted_array = $_POST['sorted_array'];
			try {
				$sorted_array_ele = explode(',', $sorted_array);
				echo "<pre>";
				array_pop($sorted_array_ele);
				if(!empty($sorted_array_ele)){
					$sort_order = 0;
					foreach ($sorted_array_ele as $product_id) {
						$this->model_catalog_product->updateProductPosition($sort_order, $product_id);
						$sort_order++;
					}
					$msg .= "Product(s) position updated successfully!";
				}else{
					$msg .= "Something went wrong!";
				}
				
			} catch (Exception $e) {
				$msg = $e;
			}
		}
			echo json_encode(array("msg" => $msg));
			exit();
	}
}