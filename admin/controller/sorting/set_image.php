<?php
class ControllerSortingSetImage extends Controller {
	private $error = array();
	public function index()
	{
		$this->load->language('sorting/set_image');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$breadcrumbs_path = 'sorting/set_image';
		$this->getCategoryList($breadcrumbs_path);
	}

	public function getCategoryList($breadcrumbs_path){
		$data = array();
		$url = '';

		// $category_total = $this->model_catalog_category->getTotalCategories();
		// $results = $this->model_catalog_category->getParentChildCategories($filter_data);
		$all_cat_results = $this->model_catalog_category->getAllTopParentCategoriesIdsNames();
// print_r($all_cat_results); exit();
		if(count($all_cat_results) > 0){
			$select_html ='<select id="categoryselect" class="form-control">';
		   	$select_html .='<option value="0">All Categories</option>';
			$cat_status="";
			$total_product_category = "";
			$total_product_count = "";
			foreach ($all_cat_results as $value) {
			  if(count($value) > 1){

				if($value['status'] == 0){
					$cat_status = 'disabled';
				}else{
					$cat_status = '';
				}
				
		 	$get_parent_of_childs = $this->model_catalog_category->getAllParentCategoriesOfChilds($value['category_id']);
		 	if(!empty($get_parent_of_childs)){
		 	 if($get_parent_of_childs[0]['parent_id'] != 0){
		 		$get_parent_child_childs = $this->model_catalog_category->getAllParentCategoriesOfChilds($get_parent_of_childs[0]['parent_id']);
		 		if(!empty($get_parent_child_childs)){
		 			if($get_parent_child_childs[0]['parent_id'] != 0){
		 			$get_most_parent_of_childs = $this->model_catalog_category->getAllParentCategoriesOfChilds($get_parent_child_childs[0]['parent_id']);
		 		  if(!empty($get_most_parent_of_childs)){
		 			if($get_most_parent_of_childs[0]['parent_id'] != 0){

						$total_child_of_parent = $this->model_catalog_category->getAllCategoriesIds($get_most_parent_of_childs[0]['category_id']);

						$this->getChildOfParent($total_child_of_parent);

		 			}else{
		 				// print_r("I am here!!!"); exit();
		 				$select_html.= '<option class="parentopt" value="'.$get_most_parent_of_childs[0]['category_id'].'"><b>'.$get_most_parent_of_childs[0]['name'].'</b></option>';

						$total_child_of_parent = $this->model_catalog_category->getAllCategoriesIds($get_most_parent_of_childs[0]['category_id']);
						if(!empty($total_child_of_parent)){
							foreach ($total_child_of_parent as $sub_cat) {
							$select_html.= '<option value="'.$sub_cat['category_id'].'" class="childopt"> -'.$sub_cat['name'].'</option>';
								// print_r(count($sub_cat)); exit();
								if(count($sub_cat) > 1){

									if($sub_cat['status'] == 0){
										$cat_status = 'disabled';
									}else{
										$cat_status = '';
									}

								/*$total_product_category = $this->model_catalog_product->getProductsByCategoryId($sub_cat['category_id']);
								$total_product_count = count($total_product_category);*/

									$get_child_child_ids = $this->model_catalog_category->getAllCategoriesIds($sub_cat['category_id']);

									if(!empty($get_child_child_ids)){
										foreach ($get_child_child_ids as $sub_sub_cat) {
											if(count($sub_sub_cat) > 1){

											if($sub_sub_cat['status'] == 0){
												$cat_status = 'disabled';
											}else{
												$cat_status = '';
											}

											/*$total_product_category = $this->model_catalog_product->getProductsByCategoryId($sub_sub_cat['category_id']);
											$total_product_count = count($total_product_category);*/

											$select_html.= '<option value="'.$sub_sub_cat['category_id'].'" > --&raquo;'.$sub_sub_cat['name'].'</option>';
										}else{
										$select_html.= '<option value="'.$sub_sub_cat['category_id'].'" > --&raquo;'.$sub_sub_cat['name'].'</option>';
									}
								  }
								 }else{
								$select_html.= '<option value="'.$sub_cat['category_id'].'" class="childopt"> -'.$sub_cat['name'].'</option>';
							}
							}else{
								$select_html.= '<option value="'.$sub_cat['category_id'].'" class="childopt"> -'.$value['name'].'</option>';
							}
						  }
						}
		 			}
			 	  }
		 		}
		 	 }
		 	}else{
		 		$select_html.= '<option class="parentopt" value="'.$value['category_id'].'"><b>'.$value['name'].'</b></option>';

					$total_child_of_parent = $this->model_catalog_category->getAllCategoriesIds($value['category_id']);
					if(!empty($total_child_of_parent)){
						foreach ($total_child_of_parent as $sub_cat) {
						$select_html.= '<option value="'.$sub_cat['category_id'].'" class="childopt"> -'.$sub_cat['name'].'</option>';
							// print_r(count($sub_cat)); exit();
							if(count($sub_cat) > 1){

								if($sub_cat['status'] == 0){
									$cat_status = 'disabled';
								}else{
									$cat_status = '';
								}

								$get_child_child_ids = $this->model_catalog_category->getAllCategoriesIds($sub_cat['category_id']);

								if(!empty($get_child_child_ids)){
									foreach ($get_child_child_ids as $sub_sub_cat) {
										if(count($sub_sub_cat) > 1){

										if($sub_sub_cat['status'] == 0){
											$cat_status = 'disabled';
										}else{
											$cat_status = '';
										}

										/*$total_product_category = $this->model_catalog_product->getProductsByCategoryId($sub_sub_cat['category_id']);
										$total_product_count = count($total_product_category);*/

										$select_html.= '<option value="'.$sub_sub_cat['category_id'].'" > --&raquo;'.$sub_sub_cat['name'].'</option>';
									}else{
									$select_html.= '<option value="'.$sub_sub_cat['category_id'].'" > --&raquo;'.$sub_sub_cat['name'].'</option>';
								}
							  }
							 }else{
							$select_html.= '<option value="'.$sub_cat['category_id'].'" class="childopt"> -'.$sub_cat['name'].'</option>';
						}
						}else{
							$select_html.= '<option value="'.$sub_cat['category_id'].'" class="childopt"> -'.$value['name'].'</option>';
						}
					  }
					}
	  }
		}
		}else{
		  	$select_html.= '<option class="parentopt" value="'.$value['category_id'].'"><b>'.$value['name'].'</b></option>';
	   }
	  }
	  $select_html.='</select>';
	}
// print_r($select_html); exit();
	$data['select_html'] = $select_html;

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link($breadcrumbs_path, 'user_token=' . $this->session->data['user_token'] . $url, true)
		);
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('sorting/set_image', $data));
	}

	public function loadCategoryProducts(){

	$this->load->model('catalog/product');
	$this->load->model('catalog/manufacturer');

	if(($this->request->server['REQUEST_METHOD'] == 'POST')){
		
		$loadcategoryitems = $_POST['loadcategoryitems'];
		$pagename = $_POST['pagename'];
		$pagename = basename($_POST['pagename']);
		// print_r($pagename); exit();
		$page = $_POST['currentpage'];
		// print_r($page); exit();

		$ServerPath = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";

		$limit = "";
		$category_products = "";

		if(trim($page == "single page")){
			$limit = "0, 50";
		}else if(trim($page == "view all")){
			$limit = 0;
		}else{
			$limit = (($page-1)*50).",".(50);
		}

		$total_category_products = $this->model_catalog_product->getProductsByCategoryId($loadcategoryitems);

		if($limit === 0){
			$limit = count($total_category_products);
		}

		$category_products = $this->model_catalog_product->getProductsByCategoryIdLimit($loadcategoryitems, $limit);

		$number_of_pages = ceil(count($total_category_products) / 50);
		$pagination= '<div class="row"><div class="pagination">';
		if($number_of_pages > 0){
			for($i=1; $i<=$number_of_pages; $i++){
				$pagination .='<a href="Javascript:;" data-totalpage = "'.$number_of_pages.'" categoryid="'.$loadcategoryitems.'" pageno="'.$i.'" class="pageno">'.$i.'</a>';
			}
			if($number_of_pages > 1){
			$pagination .= '<a href="javascript:void(0)" id="view_all_items"><button style="background-color: #000;border: 1px solid blue; color: #fff; height: 28px;">View all</button></a>';
		     }
		}
		$pagination .='<div id="page_no" style="float: right;margin-right: 60%;"></div></div></div>';

		$plists = "";
		if(!empty($category_products)){
			foreach ($category_products as $category_pro) {
				$manufacturer_detail = $this->model_catalog_manufacturer->getManufacturer($category_pro['manufacturer_id']);
				if(!empty($manufacturer_detail)){
					$_productBrandsName = $manufacturer_detail['name'];
					$brand_image = $manufacturer_detail['image'];
				}else{
					$manufacturer_detail['name'] = "";
					$manufacturer_detail['image'] = "catalog/coming.png";
				}

				$product_url = $ServerPath.'/index.php?route=product/product&path='.$category_pro['category_id'].'&product_id='.$category_pro['product_id'].'';
				$product_image = $this->model_catalog_product->getProductImages($category_pro['product_id']);
				$price = $category_pro['price'];
				$pname = $category_pro['name'];
				$itemNo = $category_pro['sku'];
				$sku = $category_pro['sku'];
				$pid = $category_pro['product_id'];
				$purl = $product_url;
				$_productModelName = $category_pro['model'];
				$mainimage = $ServerPath.'/image/'.$category_pro['image'];
				
				$pstatus= $category_pro['status'];
				$pstatus2 = $pstatus;
				if($pstatus == 0){
				$pstatus = 'Disabled';
				$status_css = 'danger';
				$fa = 'fa-times';
				}else{
				$pstatus = 'Enabled';
				$status_css = 'success';
				$fa = 'fa-check';
				}
				
				$pbrand = $manufacturer_detail['name'];
				$brand_image = $manufacturer_detail['image'];
				$gallery_imgs = '';
				$extra_product_img = array();
				if(!empty($product_image)){
					foreach ($product_image as $image) {
						$gimp = $image['image'];
						$gallery_imgs .= '<a href="" class="editproduct"><img src="'.$gimp.'"/></a>';
					}
				}else{
					$gallery_imgs = '';
				}
				
			$plists .='<div class="col-sm-4 col-md-3 drop-area improduct" id="'.$pid.'" sku="'.$sku.'">
						<div class="thumbnail_custom" id="drop_'.$pid.'" style="padding-top: 246px;color: #c1bbbb;cursor:move;margin:1px 0;border: 1px solid #5bc0de;height: 588px;display:none;">
	       					 <h3>Drop image here</h3>
	    				</div>
						<div class="thumbnail" id="thumbnail_'.$pid.'">

						<h4 class="text-center">';
							if(file_exists($brand_image)){
							$plists .='<img width="50" height="50" src="'.$ServerPath.'/image/'.$brand_image.'"/>'; 
						    }else{
						    	$plists .='<img width="50" height="50" src="'.$ServerPath.'/image/'.$brand_image.'" data="'.$ServerPath.'/image/'.$brand_image.'"/>';
						    }
							$plists .='<span class="label label-info productmodel" title="'.$_productModelName.'">'.$_productModelName.'</span></h4>
							<a href="'.$purl.'" target="_blank" class="">';
							if($mainimage != "")
							{
								$plists .='<img src="'.$mainimage.'" width="150" height="50" class="img-responsive baseimg'.$pid.'" id="'.$purl.'">';
							}else{
								$plists .='<img src="'.$ServerPath.'/image/catalog/coming.png" width="150" height="50" class="img-responsive baseimg'.$pid.'" id="'.$purl.'">';
							}
							$plists .='</a>
							<!-- product image gallery test -->
							
							';
							if(trim($pagename === "set_image"))
							{
								$apple ='<div class="row"><div class="col-md-4">
									<div pid="'.$pid.'" class="mainthumb'.$pid.'">100</div>
									<br>
									<div pid="'.$pid.'" class="gthumb'.$pid.' gthumbclick">1</div>
									<div pid="'.$pid.'" class="gthumb'.$pid.' gthumbclick">2</div>
									<div pid="'.$pid.'" class="gthumb'.$pid.' gthumbclick">3</div>
								</div>
								</div>
								<!-- end -->';
								$gallery_images = '';

							if(!empty($product_image)){
								foreach ($product_image as $image) 
								{
									$gimp 		= $ServerPath."/image/".$image['image'];
									$filename 	= $image['image'];

									$gallery_images .= '<div class="thumbnail2 maingallerydelete" id="'.md5($gimp).'"><img pid="'.$pid.'" class="gthumb'.$pid.' gthumbclick mediagalleryimage" imgurl="'.$gimp.'" imgfile="'.$filename.'" width="50" height="50" src="'.$gimp.'"/><a href="Javascript:;" todelete="'.md5($filename).'" imgfile="'.$filename.'" class="deleteMainGalleryImage" pid="'.$pid.'" >x</a> </div>';
									//$gallery_imgs .= '<a href="" class="editproduct"><img src="'.$gimp.'"/></a>';
								}
							}
								$plists .= '<div class="row"><div class="col-md-12" id="galleryimages_'.$pid.'">'.$gallery_images.'</div></div>';
							}
							$plists .= '<div class="caption">
								<div class="row product-info-styles">
									<div class="col-md-6 col-xs-6">
										<span>'.$pid.', $'.(float)$price.'</span> 
									</div>
									<div class="col-md-6 col-xs-6">
									<button type="button" class="btn btn-'.$status_css.' btn-xs update_status_'.$pstatus.'" pid="'.$pid.'">
										<span><i  class="fa '.$fa.'"></i></span>&nbsp;&nbsp;'.$pstatus.'
									</button>
									</div>
								</div>
								<div class="row product-info-styles">
									<div class="col-md-6 col-sm-6 col-xs-6">
										<span >SKU</span> 
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
										<span>'.$sku.'</span> 
									</div>
								</div>
								
								<div class="row product-info-styles">
									<div class="col-md-12 col-sm-12 text-center">
										<button type="button" class="btn btn-warning btn-xs productname" title="'.$_productBrandsName.'">
										'.$_productBrandsName.'
										</button></div></div>';
									if($pagename == "set_image")
									{
									$plists .='<div class="row"><div class="col-md-6"><a target="_blank" href="https://www.google.co.in/search?q='.$_productBrandsName.'+'.$_productModelName.'+image" pid="'.$pid.'" class="googlepicture"><img src="'.$ServerPath.'/image/catalog/searchimage.png" class="tester"/></a></div>';
									$plists .='<form name="" method="post" action="#" class=""><div class="col-md-6 col-sm-12">
										<label class="custom-file-upload">
										    Add Image
										    <input  class="addImage" type="file" id="file" name="setname" sku="'.$sku.'" pid="'.$pid.'"/>
										</label>
										
										</div></form></div>';
									}
									$plists .='</div>
								</div>';

								/*if($pagename == "set_image")
								{
								$plists .='<div class="col-md-6 col-sm-12">
								<label class="custom-file-upload">
								    Add Image
								    <input  class="addImage" type="file" name="setname" sku="'.$sku.'" pid="'.$pid.'"/>
								</label>
								</div>';
								}*/

								$plists .='<p> </p>
							</div>
						</div>
					</div>
					';
				}
				$plists .= '<style>
					div.editpictures {
						background: blue;
						width: 160px;
						height: 35px;
					}
					</style>';
			}
			else{
				$plists .= '<h4 style=" margin-bottom: 5%; " class="text-center">There are no products in this category!</h4>';
			}
				echo json_encode(array('pagination'=> $pagination, 'items'=> $plists));
				exit;
		}
	}

	public function deleteProductImage(){
		$this->load->model('catalog/product');

		if(($this->request->server['REQUEST_METHOD'] == 'POST')){
			$msg = '';
			$imgfilename = $_POST['imgfile'];
			$product_id = $_POST['pid'];
			if($imgfilename != "" && $product_id != ""){
				$this->model_catalog_product->deleteProductImage($product_id, $imgfilename);
				$msg = "Image Deleted!";
			}else{
				$msg = "Something went wrong!";
			}
			echo json_encode(array('msg'=> $msg));
				exit();
		}
	}

	public function updateProductStatus(){
		$this->load->model('catalog/product');

		if(($this->request->server['REQUEST_METHOD'] == 'POST')){
			$msg = '';
			$status = $_POST['changestatus'];
			$product_id = $_POST['pid'];
			$product_status = "";

			if($status == 0){
				$product_status = "Disabled";
			}else{
				$product_status = "Enabled";
			}

			if($status != "" && $product_id != ""){
				$this->model_catalog_product->updateProductStatus($status, $product_id);
				$msg = "Product ".$product_status."!";
			}else{
				$msg = "Something went wrong!";
			}
			echo json_encode(array('msg'=> $msg));
				exit();
		}
	}

	public function addProductImage(){
		
		$this->load->model('catalog/product');
		$msg = "";
		if(($this->request->server['REQUEST_METHOD'] == 'POST')){
			$msg = '';
			$sku = $_POST['sku'];
			$pid = $_POST['pid'];
			// print_r($sku." ".$pid); exit();
			// $sku_id = substr($sku, 4);
			try {
				if($pid != ""){
					$new_image = "catalog/all_product_images/".$_FILES["image"]["name"];
					$dir = "/var/www/html/image/catalog/all_product_images/";
					move_uploaded_file($_FILES["image"]["tmp_name"], $dir. $_FILES["image"]["name"]);
					
					$this->model_catalog_product->addProductImage($pid, $new_image);

					$msg = "Product Image Added Successfully!!!";
				}
				else{
					$msg = "Something went wrong!!!";
				}
				
			} catch (Exception $e) {
				$msg = $e;
			}

			echo json_encode(array('msg'=> $msg));
			exit();
		}
	}

	public function addDragableImage(){
		$this->load->model('catalog/product');
		$msg = "";
	 if(($this->request->server['REQUEST_METHOD'] == 'POST')){
	 	try {
		 	$pid = $_POST['pid'];
		 	$url = $_POST['image'];

 		  if($pid != ""){

 		  	if (strpos($url, '?') !== false) {
			    $t = explode('?', $url);
			    $url = $t[0];          
			}    

		 	$pathinfo = pathinfo($url);
		 	$image_name = $pathinfo['filename'].'.'.$pathinfo['extension'];
			$img = '/var/www/html/image/catalog/all_product_images/'.$image_name;
			$new_image = "catalog/all_product_images/".$image_name;

			file_put_contents($img, file_get_contents($url));

			$this->model_catalog_product->addProductImage($pid, $new_image);

				$msg = "Product Image Added Successfully!!!";
			}
			else{
				$msg = "Something went wrong!!!";
			}
	 		
	 	} catch (Exception $e) {
	 		$msg = $e;
	 	}
	 	echo json_encode(array('msg'=> $msg));
			exit();
		}
	}

	public function setProductBaseImage(){
		$this->load->model('catalog/product');
		$msg = "";
	 if(($this->request->server['REQUEST_METHOD'] == 'POST')){
	 	$product_id = $_POST['pid'];
	 	$base_image = $_POST['imgfile'];
	 	try {

	 		if($product_id != "" && $base_image != ""){
	 			$this->model_catalog_product->setProductBaseImage($product_id, $base_image);

	 			$msg = "Base image successfully added!";
	 		}else{
	 			$msg = "Something went wrong!";
	 		}
	 		
	 	} catch (Exception $e) {
	 		$msg = $e;
	 	}
	 	echo json_encode(array('msg'=> $msg));
			exit();
	 }
	}
}