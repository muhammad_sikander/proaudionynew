<?php
class ControllerCatalogBrand extends Controller {
	private $error = array();

	public function index() {
		

$url = "https://tmppro.com/brands";
$ch1= curl_init();
curl_setopt ($ch1, CURLOPT_URL, $url );
curl_setopt($ch1, CURLOPT_HEADER, 0);
curl_setopt($ch1,CURLOPT_VERBOSE,1);
curl_setopt($ch1, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36');
curl_setopt ($ch1, CURLOPT_REFERER,'http://www.google.com');  //just a fake referer
curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch1,CURLOPT_POST,0);
curl_setopt($ch1, CURLOPT_FOLLOWLOCATION, 20);
curl_setopt($ch1,CURLOPT_SSL_VERIFYPEER, false);
$strCookie = 'tmppro_sess=31g7m313po63dmhfgh04ndmsqjpk86rp';
curl_setopt( $ch1, CURLOPT_COOKIE, $strCookie );
$page= curl_exec($ch1);

function get_all_brands($html_string, $get_attrs = FALSE)
{
	$ids = array();
	$string = $html_string;
	//enable user error handling
	libxml_use_internal_errors(true);
	$doc = new DOMDocument();
	$doc->loadHTML($string);
	$classname="brandLink";
    $finder = new DomXPath($doc);
    $product_div = $finder->query("//*[contains(@class, '$classname')]");
    // print_r($product_div); exit();
	//$images = $doc->getElementsByClassName('wrap_product');
	// $all_images_name = new array();
	foreach ($product_div as $pro_div) {
	  $ids[] = $pro_div->getAttribute('title') . "\n";
	}
	// foreach($string->find('wrap_product ') as $element) 
 //       $name[] = $element->inv;
	return $ids;
}

function get_all_ids($html_string, $get_attrs = FALSE)
{
	$ids = array();
	$string = $html_string;
	//enable user error handling
	libxml_use_internal_errors(true);
	$doc = new DOMDocument();
	$doc->loadHTML($string);
	$classname="wrap_product";
    $finder = new DomXPath($doc);
    $product_div = $finder->query("//*[contains(@class, '$classname')]");
	foreach ($product_div as $pro_div) {
	  $ids[] = $pro_div->getAttribute('inv') . "\n";
	}
	return $ids;
}
function getTableData($htmlContent){
	$DOM = new DOMDocument();
	$DOM->loadHTML($htmlContent);
	
	$Header = $DOM->getElementsByTagName('th');
	$Detail = $DOM->getElementsByTagName('td');

    //#Get header name of the table
	foreach($Header as $NodeHeader) 
	{
		$aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
	}
	$i = 0;
	$j = 0;
	foreach($Detail as $sNodeDetail) 
	{
		$aDataTableDetailHTML[$j][] = trim($sNodeDetail->textContent);
		$i = $i + 1;
		$j = $i % count($aDataTableHeaderHTML) == 0 ? $j + 1 : $j;
	}
	return $aDataTableDetailHTML[0];
} 
function get_images($html_string, $get_attrs = FALSE)
{
    $names = array();
	$string = $html_string;
	// enable user error handling
	libxml_use_internal_errors(true);
	$doc = new DOMDocument();
	$doc->loadHTML($string);
	$images = $doc->getElementsByTagName('img');
	// $all_images_name = new array();
	foreach ($images as $image) {
	  $all_images_name = $image->getAttribute('alt') . "\n";
	  $data = $all_images_name;
	  $names[] = $whatIWant = substr($data, strpos($data, ":") + 1); 
	}
	return $names;
}
function collect_data($detail_page){
if (isset(explode('<span class="modelNumber">',explode('</span><h2>',$detail_page)[0])[1])) {
$collect_data['modelnumber'] = explode('<span class="modelNumber">',explode('</span><h2>',$detail_page)[0])[1] ;

}else{
	$collect_data['modelnumber']="";
}
if (isset(explode('</span><h2><span>',explode('</span></h2></div>',$detail_page)[0])[1])) {
	$collect_data['shortdescription'] = explode('</span><h2><span>',explode('</span></h2></div>',$detail_page)[0])[1];
}else{
	$collect_data['shortdescription']="";
}
if (isset(explode('<div id="productView_frame1" class="productView_frame_cont"><ul>',explode("</ul></div><div class='product",$detail_page)[0])[1])) {
	$extra_image3 = explode('<div id="productView_frame1" class="productView_frame_cont">',explode("</div><div class='product",$detail_page)[0])[1];
	$collect_data['all_images'] = get_images($extra_image3);
	// print_r($collect_data['all_images']); exit();
}else{
	$collect_data['all_images'] = array();
    // print_r($collect_data['all_images']); exit();
}
if(!empty($collect_data['all_images'])){
		$collect_data['main_image'] = $collect_data['all_images'][0];
        // print_r($collect_data['main_image']);exit();
}else{
	if (isset(explode('<ul id="productView_content1" class="productView_content">',explode('</ul></div><div id="productView_thumbWrapper_cont">',$detail_page)[0])[1])) {
		$main_image3 = explode('<ul id="productView_content1" class="productView_content">',explode('</ul></div><div id="productView_thumbWrapper_cont">',$detail_page)[0])[1];
				$collect_data['main_image'] = get_images($main_image3);
				$collect_data['main_image'] = $collect_data['main_image'][0];
                // print_r($collect_data['main_image']);
	}else{
		$collect_data['main_image'] = "photo_not_available.jpg";
	}
}
if (isset(explode('<div id="product_bullet_scroll">',explode('</div></div><div class="productView_rightcontainer">',$detail_page)[0])[1])) {
$collect_data['description'] = explode('<div id="product_bullet_scroll">',explode('</div></div><div class="productView_rightcontainer">',$detail_page)[0])[1];
}else{
	$collect_data['description']="";
}
if (isset(explode('<div class="descSection"><div class="productView_longDesc_box">',explode('</div> <div class="productView_contactText">',$detail_page)[0])[1])) {
$collect_data['additional_info'] = explode('<div class="descSection"><div class="productView_longDesc_box">',explode('</div> <div class="productView_contactText">',$detail_page)[0])[1];
}else{
	$collect_data['additional_info']="";
}
if (isset(explode('<div class="productDetail">Series Name:</div><div class="productDetail_text">',explode('</div><div class="productDetail">UPC / EAN:</div>',$detail_page)[0])[1])) {
$collect_data['series_name'] = explode('<div class="productDetail">Series Name:</div><div class="productDetail_text">',explode('</div><div class="productDetail">UPC / EAN:</div>',$detail_page)[0])[1];
}else{
	$collect_data['series_name']="";
}
if (isset(explode('<div class="productDetail">Brand Name</div><div class="productDetail_text">',explode('</div><div class="productDetail">Model Number:</div>',$detail_page)[0])[1])) {
$collect_data['brand_name'] = explode('<div class="productDetail">Brand Name</div><div class="productDetail_text">',explode('</div><div class="productDetail">Model Number:</div>',$detail_page)[0])[1];
}else{
	$collect_data['brand_name']="";
}
if (isset(explode('<div class="productView_spec"><a href="https://tmppro.com/specsheets/',explode('" target="_blank" rel="specSheet">',$detail_page)[0])[1])) {
$collect_data['specsheet'] = explode('<div class="productView_spec"><a href="https://tmppro.com/specsheets/',explode('" target="_blank" rel="specSheet">',$detail_page)[0])[1];
}else{
	$collect_data['specsheet']="";
}
if (isset(explode('<div class="wrap_price_map"><div>List: $',explode('</div><div>MAP:',$detail_page)[0])[1]) || isset(explode('<div class="wrap_price_map"><div>List: $',explode('</div><div>UPP:',$detail_page)[0])[1]) ) 
{
	if (isset(explode('<div class="wrap_price_map"><div>List: $',explode('</div><div>MAP:',$detail_page)[0])[1]))
	{
	$collect_data['msrp'] = explode('<div class="wrap_price_map"><div>List: $',explode('</div><div>MAP:',$detail_page)[0])[1];
	}else{
		$collect_data['msrp'] = explode('<div class="wrap_price_map"><div>List: $',explode('</div><div>UPP:',$detail_page)[0])[1];
	}
}elseif(isset(explode('<div class="productDetail">List:</div><div class="productDetail_text">$',explode('</div><div class="productDetail">Short',$detail_page)[0])[1])){
	$collect_data['msrp'] = explode('<div class="productDetail">List:</div><div class="productDetail_text">$',explode('</div><div class="productDetail">Short',$detail_page)[0])[1];
	
}else{
	$collect_data['msrp']=0;
}
if (isset(explode('</div><div>MAP: $',explode('</div></div><div class="wrap_price_inv">',$detail_page)[0])[1]) || isset(explode('</div><div>UPP: $',explode('</div></div><div class="wrap_price_inv">',$detail_page)[0])[1])) {
	if(isset(explode('</div><div>MAP: $',explode('</div></div><div class="wrap_price_inv">',$detail_page)[0])[1]))
	{
		$collect_data['map'] = explode('</div><div>MAP: $',explode('</div></div><div class="wrap_price_inv">',$detail_page)[0])[1];
	}else{
		$collect_data['map'] = explode('</div><div>UPP: $',explode('</div></div><div class="wrap_price_inv">',$detail_page)[0])[1];
	}
}else{
	$collect_data['map']=0;
}
if (isset(explode('<div class="wrap_price_right pv_oss"><div class="availabilityFieldNew"><p style="">Availability: ',explode('</span></p></div><p class="wrap_price_dealerprice',$detail_page)[0])[1])) {
$collect_data['stock'] = explode('<div class="wrap_price_right pv_oss"><div class="availabilityFieldNew"><p style="">Availability: ',explode('</span></p></div><p class="wrap_price_dealerprice',$detail_page)[0])[1];
	$collect_data['stock'] = strip_tags($collect_data['stock']);
	if($collect_data['stock'] == "In Stock"){
	 	$collect_data['stock'] = 1;
	 	$collect_data['qty'] = 100;
	 }else{
	 	$collect_data['stock'] = 0;
	 	$collect_data['qty'] = 0;
	 }
}else{
	$collect_data['stock'] = 0;
	$collect_data['qty'] = 0;
}
if (isset(explode('<b>Retail Package Measurements &amp; Shipping Weights</b></div>',explode('</div></div></div><div class="descSectionthree descSection">',$detail_page)[0])[1])) {
$collect_data['dimension'] = explode('<b>Retail Package Measurements &amp; Shipping Weights</b></div>',explode('</div></div></div><div class="descSectionthree descSection">',$detail_page)[0])[1];
	 $data = getTableData($collect_data['dimension']);
	 $collect_data['length'] = $data[2];
	 $collect_data['width'] = $data[3];
	 $collect_data['height'] = $data[4];
	 $collect_data['Weight'] = $data[5];
}else{
	 $collect_data['length'] = 0;
	 $collect_data['width'] = 0;
	 $collect_data['height'] = 0;
	 $collect_data['Weight'] = 0;
}
return $collect_data;
}

if(isset(explode('<div class="brandPageDisplay" ><ul class="brandPageBoxedIn"><li class="">',explode('</li></ul></div></div></div>', $page)[0])[1])){
$brandlogos = explode('<div class="brandPageDisplay" ><ul class="brandPageBoxedIn"><li class="">',explode('</li></ul></div></div></div>', $page)[0])[1];
	$collect_data['logo_main'] = get_all_brands($brandlogos);
	$collect_data['logo_main'] = $collect_data['logo_main'];
	// print_r($collect_data['logo_main']); exit();
}

if (isset(explode('<div id="mainProductBoxWrapper">',explode('</div><div class="pagination_bot container">',$page)[0])[1])) {
$all_products_per_page_ids = explode('<div id="mainProductBoxWrapper">',explode('</div><div class="pagination_bot container">',$page)[0])[1];
		$all_products_per_page_ids = get_all_ids($all_products_per_page_ids);
		// print_r($all_products_per_page_ids); exit();
}else{
	$all_products_per_page_ids="";
}
if (isset(explode('Current Category: </span><span class="bold">',explode('</span><a href="https://tmppro.com/products/brand"',$page)[0])[1])) {
$category_name = explode('Current Category: </span><span class="bold">',explode('</span><a href="https://tmppro.com/products/brand"',$page)[0])[1];

}else{
	$category_name="";
}
$category_ids= array(1674);

function searchForId($value, $array) {
   foreach ($array as $key => $val) {
       if ($val['label'] === $value) {
           return $val['value'];
       }
   }
   return null;
}

if (isset(explode('<div class="pagination_top container">',explode('</div><div id="mainProductBox" class="productContainer productMatrix container"',$page)[0])[1])) {
$page_no = explode('<div class="pagination_top container">',explode('</div><div id="mainProductBox" class="productContainer productMatrix container"',$page)[0])[1];
		// echo htmlspecialchars($page_no); exit();
		$page_no = substr_count($page_no,'<li>');
}else{
	$page_no = null;
}
// print_r($page_no); exit();
if($page_no != null || count($collect_data['logo_main']) > 0 ){
	if($page_no > 0){
		for($i=1; $i<=1; $i++) {

			$pagecurl = curl_init();
			curl_setopt($pagecurl, CURLOPT_URL, $url.'/page/'.$i);
			curl_setopt($pagecurl, CURLOPT_HEADER, 0);
			curl_setopt($pagecurl,CURLOPT_VERBOSE,1);
			curl_setopt($pagecurl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36');
			curl_setopt ($pagecurl, CURLOPT_REFERER,'http://www.google.com');
			curl_setopt($pagecurl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($pagecurl,CURLOPT_POST,0);
			curl_setopt($pagecurl, CURLOPT_FOLLOWLOCATION, 20);
			curl_setopt($pagecurl,CURLOPT_SSL_VERIFYPEER, false);
			$strCookie = 'tmppro_sess=31g7m313po63dmhfgh04ndmsqjpk86rp';    
			curl_setopt( $pagecurl, CURLOPT_COOKIE, $strCookie );
			$sub_page = curl_exec($pagecurl);
			get_all_ids($sub_page);

			if (isset(explode('<div id="mainProductBoxWrapper">',explode('</div><div class="pagination_bot container">',$sub_page)[0])[1])) {
				$all_products_sub_page_ids = explode('<div id="mainProductBoxWrapper">',explode('</div><div class="pagination_bot container">',$sub_page)[0])[1];
							$all_products_sub_page_ids = get_all_ids($all_products_sub_page_ids);
							// print_r($all_products_sub_page_ids);exit();
							// array_splice($all_products_sub_page_ids,2,49);
						  // print_r($all_products_sub_page_ids); exit();
				if(count($all_products_sub_page_ids) > 0){
					
					foreach ($all_products_sub_page_ids as $detail_id) {
						$page_path ="https://tmppro.com/products/view/".trim($detail_id);
						 // echo $page_path; exit();
						$detailpath = $page_path;
						$detailpagecurl = curl_init($detailpath);
                        curl_setopt($detailpagecurl, CURLOPT_HEADER, 0);
                        curl_setopt($detailpagecurl,CURLOPT_VERBOSE,1);
                        curl_setopt($detailpagecurl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36');
                        curl_setopt ($detailpagecurl, CURLOPT_REFERER,'http://www.google.com');
                        curl_setopt($detailpagecurl, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($detailpagecurl,CURLOPT_POST,0);
                        curl_setopt($detailpagecurl, CURLOPT_FOLLOWLOCATION, 20);
                        curl_setopt($detailpagecurl,CURLOPT_SSL_VERIFYPEER, false);
                        $strCookie = 'tmppro_sess=31g7m313po63dmhfgh04ndmsqjpk86rp';    
                        curl_setopt( $detailpagecurl, CURLOPT_COOKIE, $strCookie );
						$detail_page = curl_exec($detailpagecurl);
						$all_data = collect_data($detail_page);

						if(count($all_data['all_images'] >  0)){
							$productDataCount = array();
							foreach ($all_data['all_images'] as $imgval) {
								$productDataCount[] = $productData = array(
										'image' => 'catalog/imagetestingfolder/'.trim($imgval),
									    'sort_order' => ''
								);
							  }
						}
						if(count($all_data['all_images'] ==  0)){
								if(trim($all_data['main_image']) == "photo_not_available.jpg")
								{
									$changemainimg = "catalog/coming.png";
									$all_data['main_image'] = $changemainimg;
								}
								else{
									$origmainimg = 'catalog/imagetestingfolder/'.trim($all_data['main_image']);
									$all_data['main_image'] = $origmainimg;
								}
						}

				$this->load->language('catalog/manufacturer');
				$this->document->setTitle($this->language->get('heading_title'));
				$this->load->model('catalog/manufacturer');

				$productData = array(
				    'name' => $all_data['brand_name'],
				    'manufacturer_store' => array(
				    	'0' => '0'
				    ),
				    'image' => 'catalog/demo/manufacturer/proaudiony_brands/brandimages/'.trim($all_data['brand_name']).'.jpg',
				    'sort_order' => '',
				    'manufacturer_seo_url' => array(
				    	'0'=>array(
				    		'1' => ''
				    	),
				    ),
				);
		print_r($productData); exit();
				$this->model_catalog_manufacturer->addManufacturer($productData);
				print_r("Success");     
					}
				}	 
			}else{
				$all_products_per_page_ids="fdsfsd";
			}
			echo "<script>console.log(page no :".$i."done);</script>";
		}
		
	}else{
		if(count($collect_data['logo_main']) > 0){
			// array_splice($collect_data['logo_main'],0,42);
			// print_r($collect_data['logo_main']); exit();
			foreach ($collect_data['logo_main'] as $detail_id) {
				$origbrandlogo = 'catalog/brandimages/'.trim(str_replace(" ","-",strtolower($detail_id))).'.jpg';
				// print_r($origbrandlogo); exit();
				$origbrandlogo_check = '/var/www/html/image/'.trim($origbrandlogo);
				// print_r($origbrandlogo_check); exit();
				if(file_exists($origbrandlogo_check)){
					$brand_image = $origbrandlogo;
				}else{
					$brand_image = "catalog/coming.png";
				}
// print_r($brand_image); exit();
				$this->load->language('catalog/manufacturer');
				$this->document->setTitle($this->language->get('heading_title'));
				$this->load->model('catalog/manufacturer');

				$productData = array(
				    'name' => $detail_id,
				    'manufacturer_store' => array(
				    	'0' => '0'
				    ),
				    'image' => $brand_image,
				    'sort_order' => '',
				    'manufacturer_seo_url' => array(
				    	'0'=>array(
				    		'1' => ''
				    	),
				    ),
				);
		// print_r($productData); exit();
				$this->model_catalog_manufacturer->addManufacturer($productData);
				// print_r("Success");
		
			}
		}
	}
}
echo "all done"; exit();	

	}

}