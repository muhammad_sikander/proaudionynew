<?php
class ControllerCatalogScraping extends Controller {
	private $error = array();

public function index() {

$url = "https://tmppro.com/products/category/handheldvocal";
$ch1 = curl_init();
curl_setopt ($ch1, CURLOPT_URL, $url );
curl_setopt($ch1, CURLOPT_HEADER, 0);
curl_setopt($ch1,CURLOPT_VERBOSE,1);
curl_setopt($ch1, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36');
curl_setopt ($ch1, CURLOPT_REFERER,'http://www.google.com');  //just a fake referer
curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch1,CURLOPT_POST,0);
curl_setopt($ch1, CURLOPT_FOLLOWLOCATION, 20);
curl_setopt($ch1,CURLOPT_SSL_VERIFYPEER, false);
$strCookie = 'tmppro_sess=vp0314okpe2o5ejd6k8ehkna6ob3rre0';
curl_setopt( $ch1, CURLOPT_COOKIE, $strCookie );
$page= curl_exec($ch1);
// print_r($page); exit();
function get_all_ids($html_string, $get_attrs = FALSE)
{
	$ids = array();
	$string = $html_string;
	//enable user error handling
	libxml_use_internal_errors(true);
	$doc = new DOMDocument();
	$doc->loadHTML($string);
	$classname="wrap_product";
    $finder = new DomXPath($doc);
    $product_div = $finder->query("//*[contains(@class, '$classname')]");
	foreach ($product_div as $pro_div) {
	  $ids[] = $pro_div->getAttribute('inv') . "\n";
	}
	return $ids;
}
function getTableData($htmlContent){
	$DOM = new DOMDocument();
	$DOM->loadHTML($htmlContent);
	
	$Header = $DOM->getElementsByTagName('th');
	$Detail = $DOM->getElementsByTagName('td');

    //#Get header name of the table
	foreach($Header as $NodeHeader) 
	{
		$aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
	}
	$i = 0;
	$j = 0;
	foreach($Detail as $sNodeDetail) 
	{
		$aDataTableDetailHTML[$j][] = trim($sNodeDetail->textContent);
		$i = $i + 1;
		$j = $i % count($aDataTableHeaderHTML) == 0 ? $j + 1 : $j;
	}
	return $aDataTableDetailHTML[0];
} 
function get_images($html_string, $get_attrs = FALSE)
{
    $names = array();
	$string = $html_string;
	// enable user error handling
	libxml_use_internal_errors(true);
	$doc = new DOMDocument();
	$doc->loadHTML($string);
	$images = $doc->getElementsByTagName('img');
	// $all_images_name = new array();
	foreach ($images as $image) {
	  $all_images_name = $image->getAttribute('alt') . "\n";
	  $data = $all_images_name;
	// print_r($data); exit();
	  $names[] = $whatIWant = substr($data, strpos($data, ":") + 1); 
	}
	// print_r($names); exit();
	return $names;
}

function get_brand_image($html_string, $get_attrs = FALSE)
{
    $names = "";
	$string = $html_string;
	// enable user error handling
	libxml_use_internal_errors(true);
	$doc = new DOMDocument();
	$doc->loadHTML($string);
	$images = $doc->getElementsByTagName('img');
	// $all_images_name = new array();
	foreach ($images as $image) {
	  $all_images_name = $image->getAttribute('src') . "\n";
	  $data = $all_images_name;
	  // $names[] = $whatIWant = substr($data, strpos($data, ":") + 1); 
	}
	// print_r($data); exit();
	return $data;
}

function collect_data($detail_page){
/*if(isset(explode('<div class="similarItems"><!--Start Related Product -->',explode('<!--productBox--><!--End Related Product--></div>',$detail_page)[0])[1])){
	$related_products = explode('<div class="relatedProductContainer" style=""><div class="relatedSectionHeading">Similar Items<div class="sideBarContainer"><div class="leftSideBorder">',explode('</div><div class="rightSideBorder"></div></div></div><div class="similarItems"></div></div> <div class="seriesTable noshow">',$detail_page)[0])[1];
	// $related_products = get_all_ids($related_products);
}*/

if (isset(explode('<div class="productView_wrapper productMatrix">',explode('</div><div class="productViewContainer">',$detail_page)[0])[1])) {
$brand_image = explode('<div class="productView_wrapper productMatrix">',explode('</div><div class="productViewContainer">',$detail_page)[0])[1];
// print_r($brand_image); exit();
$collect_data['brand_image'] = get_brand_image($brand_image);
}else{
	$collect_data['brand_image'] = "";
}

if (isset(explode('<span class="modelNumber">',explode('</span><h2>',$detail_page)[0])[1])) {
$collect_data['modelnumber'] = explode('<span class="modelNumber">',explode('</span><h2>',$detail_page)[0])[1] ;

}else{
	$collect_data['modelnumber']="";
}

if (isset(explode('</span><h2><span>',explode('</span></h2></div>',$detail_page)[0])[1])) {
	$collect_data['shortdescription'] = explode('</span><h2><span>',explode('</span></h2></div>',$detail_page)[0])[1];
}else{
	$collect_data['shortdescription']="";
}
if (isset(explode('<div id="productView_frame1" class="productView_frame_cont"><ul>',explode("</ul></div><div class='product",$detail_page)[0])[1])) {
	$extra_image3 = explode('<div id="productView_frame1" class="productView_frame_cont">',explode("</div><div class='product",$detail_page)[0])[1];
	$collect_data['all_images'] = get_images($extra_image3);
	// print_r($collect_data['all_images']); exit();
}else{
	$collect_data['all_images'] = array();
    // print_r($collect_data['all_images']); exit();
}
if(!empty($collect_data['all_images'])){
		$collect_data['main_image'] = $collect_data['all_images'][0];
        // print_r($collect_data['main_image']);exit();
}else{
	if (isset(explode('<ul id="productView_content1" class="productView_content">',explode('</ul></div><div id="productView_thumbWrapper_cont">',$detail_page)[0])[1])) {
		$main_image3 = explode('<ul id="productView_content1" class="productView_content">',explode('</ul></div><div id="productView_thumbWrapper_cont">',$detail_page)[0])[1];
				$collect_data['main_image'] = get_images($main_image3);
				$collect_data['main_image'] = $collect_data['main_image'][0];
                // print_r($collect_data['main_image']);
	}else{
		$collect_data['main_image'] = "photo_not_available.jpg";
	}
}
if (isset(explode('<div id="product_bullet_scroll">',explode('</div></div><div class="productView_rightcontainer">',$detail_page)[0])[1])) {
$collect_data['description'] = explode('<div id="product_bullet_scroll">',explode('</div></div><div class="productView_rightcontainer">',$detail_page)[0])[1];
}else{
	$collect_data['description']="";
}
if (isset(explode('<div class="descSection"><div class="productView_longDesc_box">',explode('</div> <div class="productView_contactText">',$detail_page)[0])[1])) {
$collect_data['additional_info'] = explode('<div class="descSection"><div class="productView_longDesc_box">',explode('</div> <div class="productView_contactText">',$detail_page)[0])[1];
}else{
	$collect_data['additional_info']="";
}
if (isset(explode('<div class="productDetail">Series Name:</div><div class="productDetail_text">',explode('</div><div class="productDetail">UPC / EAN:</div>',$detail_page)[0])[1])) {
$collect_data['series_name'] = explode('<div class="productDetail">Series Name:</div><div class="productDetail_text">',explode('</div><div class="productDetail">UPC / EAN:</div>',$detail_page)[0])[1];
}else{
	$collect_data['series_name']="";
}
if (isset(explode('<div class="productDetail">Brand Name</div><div class="productDetail_text">',explode('</div><div class="productDetail">Model Number:</div>',$detail_page)[0])[1])) {
$collect_data['brand_name'] = explode('<div class="productDetail">Brand Name</div><div class="productDetail_text">',explode('</div><div class="productDetail">Model Number:</div>',$detail_page)[0])[1];
}else{
	$collect_data['brand_name']="";
}
if (isset(explode('<div class="productView_spec"><a href="https://tmppro.com/specsheets/',explode('" target="_blank" rel="specSheet">',$detail_page)[0])[1])) {
$collect_data['specsheet'] = explode('<div class="productView_spec"><a href="https://tmppro.com/specsheets/',explode('" target="_blank" rel="specSheet">',$detail_page)[0])[1];
}else{
	$collect_data['specsheet']="";
}
if (isset(explode('<div class="wrap_price_map"><div>List: $',explode('</div><div>MAP:',$detail_page)[0])[1]) || isset(explode('<div class="wrap_price_map"><div>List: $',explode('</div><div>UPP:',$detail_page)[0])[1]) ) 
{
	if (isset(explode('<div class="wrap_price_map"><div>List: $',explode('</div><div>MAP:',$detail_page)[0])[1]))
	{
	$collect_data['msrp'] = explode('<div class="wrap_price_map"><div>List: $',explode('</div><div>MAP:',$detail_page)[0])[1];
	}else{
		$collect_data['msrp'] = explode('<div class="wrap_price_map"><div>List: $',explode('</div><div>UPP:',$detail_page)[0])[1];
	}
}elseif(isset(explode('<div class="productDetail">List:</div><div class="productDetail_text">$',explode('</div><div class="productDetail">Short',$detail_page)[0])[1])){
	$collect_data['msrp'] = explode('<div class="productDetail">List:</div><div class="productDetail_text">$',explode('</div><div class="productDetail">Short',$detail_page)[0])[1];
	
}else{
	$collect_data['msrp']=0;
}
if (isset(explode('</div><div>MAP: $',explode('</div></div><div class="wrap_price_inv">',$detail_page)[0])[1]) || isset(explode('</div><div>UPP: $',explode('</div></div><div class="wrap_price_inv">',$detail_page)[0])[1])) {
	if(isset(explode('</div><div>MAP: $',explode('</div></div><div class="wrap_price_inv">',$detail_page)[0])[1]))
	{
		$collect_data['map'] = explode('</div><div>MAP: $',explode('</div></div><div class="wrap_price_inv">',$detail_page)[0])[1];
	}else{
		$collect_data['map'] = explode('</div><div>UPP: $',explode('</div></div><div class="wrap_price_inv">',$detail_page)[0])[1];
	}
}else{
	$collect_data['map']=0;
}
if (isset(explode('<div class="wrap_price_right pv_oss"><div class="availabilityFieldNew"><p style="">Availability: ',explode('</span></p></div><p class="wrap_price_dealerprice',$detail_page)[0])[1])) {
$collect_data['stock'] = explode('<div class="wrap_price_right pv_oss"><div class="availabilityFieldNew"><p style="">Availability: ',explode('</span></p></div><p class="wrap_price_dealerprice',$detail_page)[0])[1];
	$collect_data['stock'] = strip_tags($collect_data['stock']);
	if($collect_data['stock'] == "In Stock"){
	 	$collect_data['stock'] = 1;
	 	$collect_data['qty'] = 100;
	 }else{
	 	$collect_data['stock'] = 0;
	 	$collect_data['qty'] = 0;
	 }
}else{
	$collect_data['stock'] = 0;
	$collect_data['qty'] = 0;
}
if (isset(explode('<b>Retail Package Measurements &amp; Shipping Weights</b></div>',explode('</div></div></div><div class="descSectionthree descSection">',$detail_page)[0])[1])) {
$collect_data['dimension'] = explode('<b>Retail Package Measurements &amp; Shipping Weights</b></div>',explode('</div></div></div><div class="descSectionthree descSection">',$detail_page)[0])[1];
	 $data = getTableData($collect_data['dimension']);
	 $collect_data['length'] = $data[2];
	 $collect_data['width'] = $data[3];
	 $collect_data['height'] = $data[4];
	 $collect_data['Weight'] = $data[5];
}else{
	 $collect_data['length'] = 0;
	 $collect_data['width'] = 0;
	 $collect_data['height'] = 0;
	 $collect_data['Weight'] = 0;
}

return $collect_data;
}

if (isset(explode('<div id="mainProductBoxWrapper">',explode('</div><div class="pagination_bot container">',$page)[0])[1])) {
$all_products_per_page_ids = explode('<div id="mainProductBoxWrapper">',explode('</div><div class="pagination_bot container">',$page)[0])[1];
		$all_products_per_page_ids = get_all_ids($all_products_per_page_ids);
		// print_r($all_products_per_page_ids); exit();
}

if (isset(explode('<div id="mainProductBoxWrapper">',explode('</div><div class="pagination_bot container">',$page)[0])[1])) {
$all_products_per_page_ids = explode('<div id="mainProductBoxWrapper">',explode('</div><div class="pagination_bot container">',$page)[0])[1];
		$all_products_per_page_ids = get_all_ids($all_products_per_page_ids);
		// print_r($all_products_per_page_ids); exit();
}else{
	$all_products_per_page_ids="";
}

if (isset(explode('Current Category: </span><span class="bold">',explode('</span><a href="https://tmppro.com/products/brand"',$page)[0])[1])) {
$category_name = explode('Current Category: </span><span class="bold">',explode('</span><a href="https://tmppro.com/products/brand"',$page)[0])[1];

}else{
	$category_name="";
}

// print_r($category_name); exit();

$category_ids= array(1674);

function searchForId($value, $array) {
   foreach ($array as $key => $val) {
       if ($val['label'] === $value) {
           return $val['value'];
       }
   }
   return null;
}

if (isset(explode('<div class="pagination_top container">',explode('</div><div id="mainProductBox" class="productContainer productMatrix container"',$page)[0])[1])) {
$page_no = explode('<div class="pagination_top container">',explode('</div><div id="mainProductBox" class="productContainer productMatrix container"',$page)[0])[1];
		// echo htmlspecialchars($page_no); exit();
		$page_no = substr_count($page_no,'<li>');
}else{
	$page_no = null;
}
// print_r($all_products_per_page_ids); exit();
if($page_no != null || count($all_products_per_page_ids) > 0 ){
	if($page_no > 0){
		for($i=1; $i<=$page_no; $i++) {
			echo "<script>console.log( 'page no: " . $i . " starts' );</script>";
			$pagecurl = curl_init();
			curl_setopt($pagecurl, CURLOPT_URL, $url.'/page/'.$i);
			curl_setopt($pagecurl, CURLOPT_HEADER, 0);
			curl_setopt($pagecurl,CURLOPT_VERBOSE,1);
			curl_setopt($pagecurl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36');
			curl_setopt ($pagecurl, CURLOPT_REFERER,'http://www.google.com');
			curl_setopt($pagecurl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($pagecurl,CURLOPT_POST,0);
			curl_setopt($pagecurl, CURLOPT_FOLLOWLOCATION, 20);
			curl_setopt($pagecurl,CURLOPT_SSL_VERIFYPEER, false);
			$strCookie = 'tmppro_sess=vp0314okpe2o5ejd6k8ehkna6ob3rre0';    
			curl_setopt( $pagecurl, CURLOPT_COOKIE, $strCookie );
			$sub_page = curl_exec($pagecurl);
			get_all_ids($sub_page);
// print_r(get_all_ids($sub_page)); exit();
		if (isset(explode('<div id="mainProductBoxWrapper">',explode('</div><div class="pagination_bot container">',$sub_page)[0])[1])) {
			$all_products_sub_page_ids = explode('<div id="mainProductBoxWrapper">',explode('</div><div class="pagination_bot container">',$sub_page)[0])[1];
						$all_products_sub_page_ids = get_all_ids($all_products_sub_page_ids);
						// print_r($all_products_sub_page_ids);exit();
						// array_splice($all_products_sub_page_ids,0,34);
					  // print_r($all_products_sub_page_ids); exit();
				$count = 0;
			if(count($all_products_sub_page_ids) > 0){
				foreach ($all_products_sub_page_ids as $detail_id) {
					$count++;
					$page_path ="https://tmppro.com/products/view/".trim($detail_id);
					$detailpagecurl = curl_init();
					curl_setopt($detailpagecurl, CURLOPT_URL, $page_path);
				    curl_setopt($detailpagecurl, CURLOPT_HEADER, 0);
				    curl_setopt($detailpagecurl,CURLOPT_VERBOSE,1);
				    curl_setopt($detailpagecurl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36');
				    curl_setopt ($detailpagecurl, CURLOPT_REFERER,'http://www.google.com');
				    curl_setopt($detailpagecurl, CURLOPT_RETURNTRANSFER, 1);
				    curl_setopt($detailpagecurl,CURLOPT_POST,0);
				    curl_setopt($detailpagecurl, CURLOPT_FOLLOWLOCATION, 20);
				    curl_setopt($detailpagecurl,CURLOPT_SSL_VERIFYPEER, false);
				    $strCookie = 'tmppro_sess=vp0314okpe2o5ejd6k8ehkna6ob3rre0';    
				    curl_setopt( $detailpagecurl, CURLOPT_COOKIE, $strCookie );
					$detail_page = curl_exec($detailpagecurl);
					$all_data = collect_data($detail_page);
print_r($all_data);  exit();
	$productDataCount = array();
	if(count($all_data['all_images']) > 0){
		foreach ($all_data['all_images'] as $imgval) {
			$main_img = '/var/www/html/image/catalog/all_product_images/'.trim($imgval);
			if(file_exists($main_img)){
				$productDataCount[] = $productData = array(
					'image' => 'catalog/all_product_images/'.trim($imgval),
				    'sort_order' => ''
				);
			}else{
				$productDataCount[] = $productData = array(
					'image' => 'catalog/coming.png',
				    'sort_order' => ''
				);
			}
		  }
	}
// print_r(trim($all_data['main_image'])); exit();
	if(trim($all_data['main_image']) != " "){
		if(trim($all_data['main_image']) == "photo_not_available.jpg"){
			$changemainimg = "catalog/coming.png";
			$all_data['main_image'] = $changemainimg;
		}
		else{
			$origmainimg = 'catalog/all_product_images/'.trim($all_data['main_image']);
			$main_img = '/var/www/html/image/'.trim($origmainimg); 
			// print_r($main_img); exit();
			if(file_exists($main_img)){
				$all_data['main_image'] = $origmainimg;
				// print_r($all_data['main_image']." I am true!!!"); exit();
			}else{
				$changemainimg = "catalog/coming.png";
				$all_data['main_image'] = $changemainimg;
				// print_r($all_data['main_image']." I am false!!!"); exit();
			}
		}
	}


	$stock_in_out_check = 0;
	if($all_data['stock'] == 0){
		$stock_in_out_check = 5;
		$all_data['stock'] = $stock_in_out_check;
	}elseif($all_data['stock'] == 1){
		$stock_in_out_check = 7;
		$all_data['stock'] = $stock_in_out_check;
	}

	$product_name = "";
	if($all_data['shortdescription'] != ""){
		$product_name = $all_data['shortdescription'];
	}else{
		$product_name = $all_data['modelnumber'];
	}

/****************Fetching manufacturers from Database********************/
			
	$manufacturer['filter_name'] = $all_data['brand_name'];
	$this->load->language('catalog/manufacturer');
	$this->load->model('catalog/manufacturer');
	$manufacturer_list = $this->model_catalog_manufacturer->getManufacturers($manufacturer);
	if(!empty($manufacturer_list)){
		$manufacturer_id = $manufacturer_list[0]['manufacturer_id'];
	}else{
		if(!empty($all_data['brand_image'])){
			print_r($all_data['brand_name']); exit();
	    	$url = "https://tmppro.com".trim($all_data['brand_image']);
			$content = file_get_contents($url);
			$brand_image_name = str_replace(' ', '-', strtolower($all_data['brand_name']));
			$fp = fopen("/var/www/html/image/catalog/brandimages/".trim($brand_image_name.".jpg") , "w");
			fwrite($fp, $content);
			fclose($fp);

			sleep(5);

			$brandData = array(
			    'name' => $all_data['brand_name'],
			    'manufacturer_store' => array(
			    	'0' => '0'
			    ),
			    'image' => 'catalog/brandimages/'.trim($brand_image_name.'.jpg'),
			    'sort_order' => '',
			    'manufacturer_seo_url' => array(
			    	'0'=>array(
			    		'1' => ''
			    	),
			    ),
			);
			// print_r($brandData); exit();
			$this->model_catalog_manufacturer->addManufacturer($brandData);
			print_r($all_data['brand_name']. " New brand added successfully!!!");

			sleep(5);

			$manufacturer['filter_name'] = $all_data['brand_name'];
			$manufacturer_list = $this->model_catalog_manufacturer->getManufacturers($manufacturer);
			$manufacturer_id = $manufacturer_list[0]['manufacturer_id'];
			// print_r($manufacturer_id); exit();
	    }
	}

	// print_r($manufacturer_id. "Hi!!!"); exit();
/*************************************************************************//*


***************Fetching categories from Database********************/

				$category_name_store = array();
				$brand_category_store = array();
				$this->load->language('catalog/category');
				$this->load->model('catalog/category');
				$category_info = $this->model_catalog_category->getCategoryDescriptions(trim($category_name));
				// print_r($category_info); exit();
				$brand_category = $this->model_catalog_category->getCategoryDescriptions($all_data['brand_name']);
				// print_r($brand_category); exit();
				foreach ($category_info as $value) {
					$category_name_store[] = $value['category_id'];
				}
// print_r($category_name_store); exit();
				foreach ($brand_category as $brand_value) {
					$brand_category_store[] = $brand_value['category_id'];
				}

				$brand_product_cat = array_merge($category_name_store, $brand_category_store);
				$all_categories_category = array_unique($brand_product_cat);
// print_r($all_categories_category); exit();
/*************************************************************************/

	$this->load->language('catalog/product');
	$this->document->setTitle($this->language->get('heading_title'));
	$this->load->model('catalog/product');

	$exist_product = 'pro_'.$detail_id;
	$check_product = $this->model_catalog_product->getProductTest($exist_product);
// print_r($check_product); exit();	
	if(!empty($check_product)){
		$product_id = $check_product['product_id'];
		$product_detail_id = $this->model_catalog_product->getProductCategories($product_id);
		if(!empty($product_detail_id)){
			$merged_arrays = array_diff($product_detail_id, $all_categories_category);
			if(!empty($merged_arrays) || empty($brand_category) || !empty($brand_category)){
					$updatePro = array(
				    'model' => $all_data['modelnumber'],
				    'sku' => 'pro_'.$detail_id,
				    'upc' => '',
				    'ean' => '',
				    'jan' => '',
				    'isbn' => '',
				    'mpn' => '',
				    'location' => '',
				    'quantity' => $all_data['qty'],
				    'minimum' => '1',
				    'subtract' => '',
				    'stock_status_id' => $all_data['stock'],
				    'date_available' => '',
				    'manufacturer_id' => $manufacturer_id,
				    'shipping' => '',
				    'price' => intval(preg_replace('/[^\d.]/', '', $all_data['map'])),
				    'list_price' => intval(preg_replace('/[^\d.]/', '', $all_data['msrp'])),
				    'points' => '',
				    'weight' => $all_data['Weight'],
				    'weight_class_id' => '',
				    'length' => $all_data['length'],
				    'width' => $all_data['width'],
				    'height' => $all_data['height'],
				    'length_class_id' => '',
				    'status' => '1',
				    'tax_class_id' => '9',
				    'sort_order' => '',
				    'product_store' => array(
			    	 'product_store' => '0'
			    	),
				    // 'product_category' => $category_name_store,
				    'image' => trim($all_data['main_image']),
				    'product_image' => $productDataCount,
				    'product_description' => array(
			    	1=>array('name' => $product_name,
			    	'description' => $all_data['additional_info'],
			    	'meta_title' => 'proaudiony - '.$all_data['modelnumber']." - ". $all_data['brand_name'],
			    	'meta_description' => '',
			    	'meta_keyword' => '',
			    	'tag' => ''
			    	),
				 ),
				);
			
				$product_detail_merge = array_merge($product_detail_id, $all_categories_category);
				$product_category = array_unique($product_detail_merge);
				$updatePro['product_category'] = $product_category;
				// print_r($updatePro); exit();
				$this->model_catalog_product->editProduct($product_id, $updatePro);
				print_r($count ." Existing product has been updated ".$all_data['modelnumber']); 
				echo('</br>');
			}
		}
	}else{
		$productData = array(
		    'model' => $all_data['modelnumber'],
		    'sku' => 'pro_'.$detail_id,
		    'upc' => '',
		    'ean' => '',
		    'jan' => '',
		    'isbn' => '',
		    'mpn' => '',
		    'location' => '',
		    'quantity' => $all_data['qty'],
		    'minimum' => '1',
		    'subtract' => '',
		    'stock_status_id' => $all_data['stock'],
		    'date_available' => '',
		    'manufacturer_id' => $manufacturer_id,
		    'shipping' => '',
		    'price' => intval(preg_replace('/[^\d.]/', '', $all_data['map'])),
		    'list_price' => intval(preg_replace('/[^\d.]/', '', $all_data['msrp'])),
		    'points' => '',
		    'weight' => $all_data['Weight'],
		    'weight_class_id' => '',
		    'length' => $all_data['length'],
		    'width' => $all_data['width'],
		    'height' => $all_data['height'],
		    'length_class_id' => '',
		    'status' => '1',
		    'tax_class_id' => '9',
		    'sort_order' => '',
		    'product_store' => array(
	    	 'product_store' => '0'
	    	),
		    'product_category' => $all_categories_category,
		    'image' => trim($all_data['main_image']),
		    'product_image' => $productDataCount,
		    'product_description' => array(
	    	1=>array('name' => $product_name,
	    	'description' => $all_data['additional_info'],
	    	'meta_title' => 'proaudiony - '.$all_data['modelnumber']." - ". $all_data['brand_name'],
	    	'meta_description' => '',
	    	'meta_keyword' => '',
	    	'tag' => ''
	    	),
		 ),
		);
// print_r($productData); exit();
		$this->model_catalog_product->addProduct($productData);
		print_r($count ." New product has been Added ".$all_data['modelnumber']); echo('</br>');
	}
  }
}	 
	}else{
		$all_products_per_page_ids="fdsfsd";
	}
	echo "<script>console.log( 'page no: " . $i . " done' );</script>";
	}
		
	}else{
		// print_r($all_products_per_page_ids); exit();
		$count = 0;
		if(count($all_products_per_page_ids) > 0){
			foreach ($all_products_per_page_ids as $detail_id) {
				$count++;
				$page_path = "https://tmppro.com/products/view/".trim($detail_id);
				// $detailpagecurl = curl_init($page_path);
				$detailpagecurl = curl_init();
				curl_setopt($detailpagecurl, CURLOPT_URL, $page_path);
			    curl_setopt($detailpagecurl, CURLOPT_HEADER, 0);
			    curl_setopt($detailpagecurl,CURLOPT_VERBOSE,1);
			    curl_setopt($detailpagecurl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36');
			    curl_setopt ($detailpagecurl, CURLOPT_REFERER,'http://www.google.com');
			    curl_setopt($detailpagecurl, CURLOPT_RETURNTRANSFER, 1);
			    curl_setopt($detailpagecurl,CURLOPT_POST,0);
			    curl_setopt($detailpagecurl, CURLOPT_FOLLOWLOCATION, 20);
			    curl_setopt($detailpagecurl,CURLOPT_SSL_VERIFYPEER, false);
			    $strCookie = 'tmppro_sess=vp0314okpe2o5ejd6k8ehkna6ob3rre0';    
			    curl_setopt( $detailpagecurl, CURLOPT_COOKIE, $strCookie );
				$detail_page = curl_exec($detailpagecurl);
				$all_data = collect_data($detail_page);
// print_r($all_data); exit();
	$productDataCount = array();
	if(count($all_data['all_images']) > 0){
		foreach ($all_data['all_images'] as $imgval) {
			$main_img = '/var/www/html/image/catalog/all_product_images/'.trim($imgval);
			if(file_exists($main_img)){
				$productDataCount[] = $productData = array(
					'image' => 'catalog/all_product_images/'.trim($imgval),
				    'sort_order' => ''
				);
			}else{
				$productDataCount[] = $productData = array(
					'image' => 'catalog/coming.png',
				    'sort_order' => ''
				);
			}
		  }
	}
// print_r(trim($all_data['main_image'])); exit();
	if(trim($all_data['main_image']) != " "){
		if(trim($all_data['main_image']) == "photo_not_available.jpg")
		{
			$changemainimg = "catalog/coming.png";
			$all_data['main_image'] = $changemainimg;
		}
		else{
			$origmainimg = 'catalog/all_product_images/'.trim($all_data['main_image']);
			$main_img = '/var/www/html/image/'.trim($origmainimg); 
			// print_r($main_img); exit();
			if(file_exists($main_img)){
				$all_data['main_image'] = $origmainimg;
				// print_r($all_data['main_image']." I am true!!!"); exit();
			}else{
				$changemainimg = "catalog/coming.png";
				$all_data['main_image'] = $changemainimg;
				// print_r($all_data['main_image']." I am false!!!"); exit();
			}
		}
	}


	$stock_in_out_check = 0;
	if($all_data['stock'] == 0){
		$stock_in_out_check = 5;
		$all_data['stock'] = $stock_in_out_check;
	}elseif($all_data['stock'] == 1){
		$stock_in_out_check = 7;
		$all_data['stock'] = $stock_in_out_check;
	}

	$product_name = "";
	if($all_data['shortdescription'] != ""){
		$product_name = $all_data['shortdescription'];
	}else{
		$product_name = $all_data['modelnumber'];
	}

/****************Fetching manufacturers from Database********************/
// print_r($all_data['brand_name']); exit();
			
	$manufacturer['filter_name'] = $all_data['brand_name'];
	$this->load->language('catalog/manufacturer');
	$this->load->model('catalog/manufacturer');
	$manufacturer_list = $this->model_catalog_manufacturer->getManufacturers($manufacturer);
	if(!empty($manufacturer_list)){
		$manufacturer_id = $manufacturer_list[0]['manufacturer_id'];
	}else{
		if(!empty($all_data['brand_image'])){
			// print_r($all_data['brand_name']); exit();
	    	$url = "https://tmppro.com".trim($all_data['brand_image']);
			$content = file_get_contents($url);
			$brand_image_name = str_replace(' ', '-', strtolower($all_data['brand_name']));
			$fp = fopen("/var/www/html/image/catalog/brandimages/".trim($brand_image_name.".jpg") , "w");
			fwrite($fp, $content);
			fclose($fp);

			sleep(5);

			$brandData = array(
			    'name' => $all_data['brand_name'],
			    'manufacturer_store' => array(
			    	'0' => '0'
			    ),
			    'image' => 'catalog/brandimages/'.trim($brand_image_name.'.jpg'),
			    'sort_order' => '',
			    'manufacturer_seo_url' => array(
			    	'0'=>array(
			    		'1' => ''
			    	),
			    ),
			);
			// print_r($brandData); exit();
			$this->model_catalog_manufacturer->addManufacturer($brandData);
			print_r($all_data['brand_name']. " New brand added successfully!!!");

			sleep(5);

			$manufacturer['filter_name'] = $all_data['brand_name'];
			$manufacturer_list = $this->model_catalog_manufacturer->getManufacturers($manufacturer);
			$manufacturer_id = $manufacturer_list[0]['manufacturer_id'];
			// print_r($manufacturer_id); exit();
	    }
	}

					// print_r($manufacturer_id); exit();
/*************************************************************************/

/***************Fetching categories from Database********************/
// print_r($all_data['brand_name']); exit();
				$category_name_store = array();
				$brand_category_store = array();
				$this->load->language('catalog/category');
				$this->load->model('catalog/category');
				// print_r($category_name); exit();
				$category_info = $this->model_catalog_category->getCategoryDescriptions($category_name);
				$brand_category = $this->model_catalog_category->getCategoryDescriptions($all_data['brand_name']);
				// print_r($brand_category); exit();
				foreach ($category_info as $value) {
					$category_name_store[] = $value['category_id'];
				}

				foreach ($brand_category as $brand_value) {
					$brand_category_store[] = $brand_value['category_id'];
				}

				$brand_product_cat = array_merge($category_name_store, $brand_category_store);
				$all_categories_category = array_unique($brand_product_cat);

				// print_r($category_name_store); exit();
/*************************************************************************/

	$this->load->language('catalog/product');
	$this->document->setTitle($this->language->get('heading_title'));
	$this->load->model('catalog/product');

	$exist_product = 'pro_'.$detail_id;
	$check_product = $this->model_catalog_product->getProductTest($exist_product);

	if(!empty($check_product)){
		$product_id = $check_product['product_id'];
		$product_detail_id = $this->model_catalog_product->getProductCategories($product_id);
		if(!empty($product_detail_id)){
			$merged_arrays = array_diff($product_detail_id, $all_categories_category);
			if(!empty($merged_arrays) || empty($brand_category) || !empty($brand_category)){
					$updatePro = array(
				    'model' => $all_data['modelnumber'],
				    'sku' => 'pro_'.$detail_id,
				    'upc' => '',
				    'ean' => '',
				    'jan' => '',
				    'isbn' => '',
				    'mpn' => '',
				    'location' => '',
				    'quantity' => $all_data['qty'],
				    'minimum' => '1',
				    'subtract' => '',
				    'stock_status_id' => $all_data['stock'],
				    'date_available' => '',
				    'manufacturer_id' => $manufacturer_id,
				    'shipping' => '',
				    'price' => intval(preg_replace('/[^\d.]/', '', $all_data['map'])),
				    'list_price' => intval(preg_replace('/[^\d.]/', '', $all_data['msrp'])),
				    'points' => '',
				    'weight' => $all_data['Weight'],
				    'weight_class_id' => '',
				    'length' => $all_data['length'],
				    'width' => $all_data['width'],
				    'height' => $all_data['height'],
				    'length_class_id' => '',
				    'status' => '1',
				    'tax_class_id' => '9',
				    'sort_order' => '',
				    'product_store' => array(
			    	 'product_store' => '0'
			    	),
				    // 'product_category' => $category_name_store,
				    'image' => trim($all_data['main_image']),
				    'product_image' => $productDataCount,
				    'product_description' => array(
			    	1=>array('name' => $product_name,
			    	'description' => $all_data['additional_info'],
			    	'meta_title' => 'proaudiony - '.$all_data['modelnumber']." - ". $all_data['brand_name'],
			    	'meta_description' => '',
			    	'meta_keyword' => '',
			    	'tag' => ''
			    	),
				 ),
				);

				$product_detail_merge = array_merge($product_detail_id, $all_categories_category);
				$product_category = array_unique($product_detail_merge);
				$updatePro['product_category'] = $product_category;
				// print_r($updatePro); exit();
				$this->model_catalog_product->editProduct($product_id, $updatePro);
				print_r($count ." Existing product has been updated ".$all_data['modelnumber']); 
				echo('</br>');
			}
		}
	}else{
		$productData = array(
		    'model' => $all_data['modelnumber'],
		    'sku' => 'pro_'.$detail_id,
		    'upc' => '',
		    'ean' => '',
		    'jan' => '',
		    'isbn' => '',
		    'mpn' => '',
		    'location' => '',
		    'quantity' => $all_data['qty'],
		    'minimum' => '1',
		    'subtract' => '',
		    'stock_status_id' => $all_data['stock'],
		    'date_available' => '',
		    'manufacturer_id' => $manufacturer_id,
		    'shipping' => '',
		    'price' => intval(preg_replace('/[^\d.]/', '', $all_data['map'])),
		    'list_price' => intval(preg_replace('/[^\d.]/', '', $all_data['msrp'])),
		    'points' => '',
		    'weight' => $all_data['Weight'],
		    'weight_class_id' => '',
		    'length' => $all_data['length'],
		    'width' => $all_data['width'],
		    'height' => $all_data['height'],
		    'length_class_id' => '',
		    'status' => '1',
		    'tax_class_id' => '9',
		    'sort_order' => '',
		    'product_store' => array(
	    	 'product_store' => '0'
	    	),
		    'product_category' => $all_categories_category,
		    'image' => trim($all_data['main_image']),
		    'product_image' => $productDataCount,
		    'product_description' => array(
	    	1=>array('name' => $product_name,
	    	'description' => $all_data['additional_info'],
	    	'meta_title' => 'proaudiony - '.$all_data['modelnumber']." - ". $all_data['brand_name'],
	    	'meta_description' => '',
	    	'meta_keyword' => '',
	    	'tag' => ''
	    	),
		 ),
		);
		// print_r($productData); exit();
		$this->model_catalog_product->addProduct($productData);
		print_r($count." New product has been Added ".$all_data['modelnumber']); echo('</br>');
	}
			}
		}
		echo "<script>console.log( 'Single page done!!!' );</script>";
	}
}
echo " Products added successfully!!!"; exit();
	}
}
