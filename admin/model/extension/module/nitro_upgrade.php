<?php
class ModelExtensionModuleNitroUpgrade extends ModelExtensionModuleNitro {
  public function run_upgrade() {
      if (
          !empty($this->request->post['Nitro']['PageCache']['ClearCacheOnProductEdit']) && 
          $this->request->post['Nitro']['PageCache']['ClearCacheOnProductEdit'] == 'yes'
      ) {
          try {
              initNitroProductCacheDb();
          } catch (Exception $e) {}
      }

      $upgradeFile = getNitroUpgradeFilename();
      if (!file_exists($upgradeFile) || filemtime(__FILE__) > filemtime($upgradeFile)) {
          $this->model_extension_module_nitro->setupEventHandlers();
          touch($upgradeFile);
      }
  }
}
