<?php
	error_reporting(E_ALL);
   ini_set('display_errors', TRUE);
   ini_set('display_startup_errors', TRUE);
// phpinfo(); exit();
$url = "https://tmppro.com/products/category/handsetcords";
$ch1= curl_init();
curl_setopt ($ch1, CURLOPT_URL, $url );
curl_setopt($ch1, CURLOPT_HEADER, 0);
curl_setopt($ch1,CURLOPT_VERBOSE,1);
curl_setopt($ch1, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36');
curl_setopt ($ch1, CURLOPT_REFERER,'http://www.google.com');  //just a fake referer
curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch1,CURLOPT_POST,0);
curl_setopt($ch1, CURLOPT_FOLLOWLOCATION, 20);
curl_setopt($ch1,CURLOPT_SSL_VERIFYPEER, false);
$strCookie = 'tmppro_sess=kco1l9rj5f4n7iq94vv372bknkehe90g';
curl_setopt( $ch1, CURLOPT_COOKIE, $strCookie );
$page= curl_exec($ch1);

// print_r($page); exit();

function get_all_ids($html_string, $get_attrs = FALSE)
{
	$ids = array();
	$string = $html_string;
	libxml_use_internal_errors(true);
	$doc = new DOMDocument();
	$doc->loadHTML($string);
	$classname="wrap_product";
    $finder = new DomXPath($doc);
    $product_div = $finder->query("//*[contains(@class, '$classname')]");
	foreach ($product_div as $pro_div) {
	  $ids[] = $pro_div->getAttribute('inv') . "\n";
	}
	return $ids;
}
function getTableData($htmlContent){
	$DOM = new DOMDocument();
	$DOM->loadHTML($htmlContent);
	
	$Header = $DOM->getElementsByTagName('th');
	$Detail = $DOM->getElementsByTagName('td');

	foreach($Header as $NodeHeader) 
	{
		$aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
	}
	$i = 0;
	$j = 0;
	foreach($Detail as $sNodeDetail) 
	{
		$aDataTableDetailHTML[$j][] = trim($sNodeDetail->textContent);
		$i = $i + 1;
		$j = $i % count($aDataTableHeaderHTML) == 0 ? $j + 1 : $j;
	}
	return $aDataTableDetailHTML[0];
} 
function get_images($html_string, $get_attrs = FALSE)
{
	$names = array();
	$string = $html_string;

	libxml_use_internal_errors(true);
	$doc = new DOMDocument();
	$doc->loadHTML($string);
	$images = $doc->getElementsByTagName('img');

	foreach ($images as $image) {
	  $all_images_name = $image->getAttribute('alt') . "\n";
	  $data = $all_images_name;
	  $names[] = $whatIWant = substr($data, strpos($data, ":") + 1); 
	}
	return $names;
}

function get_product_images($page_path)
{
	$detailpagecurl = curl_init();
	curl_setopt($detailpagecurl, CURLOPT_URL, $page_path);
    curl_setopt($detailpagecurl, CURLOPT_HEADER, 0);
    curl_setopt($detailpagecurl, CURLOPT_VERBOSE, 1);
    curl_setopt($detailpagecurl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36');
    curl_setopt ($detailpagecurl, CURLOPT_REFERER,'http://www.google.com');
    curl_setopt($detailpagecurl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($detailpagecurl, CURLOPT_POST, 0);
    curl_setopt($detailpagecurl, CURLOPT_FOLLOWLOCATION, 20);
    curl_setopt($detailpagecurl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($detailpagecurl, CURLOPT_SSL_VERIFYHOST, 0);
    $strCookie = 'tmppro_sess=kco1l9rj5f4n7iq94vv372bknkehe90g';    
    curl_setopt( $detailpagecurl, CURLOPT_COOKIE, $strCookie );
	$detail_page = curl_exec($detailpagecurl);
	$all_data = collect_data($detail_page);

	if (!empty($all_data['all_images'])) {

        foreach ($all_data['all_images'] as $additional_image) {
            $url = "https://tmppro.com/productImage/".trim($additional_image);
			$content = file_get_contents($url);
			$fp = fopen("image/catalog/all_product_images/".trim($additional_image) , "w");
			fwrite($fp, $content);
			fclose($fp);
        }
	}

	elseif(!empty($all_data['main_image'])){
	   $url = "https://tmppro.com/productImage/".trim($all_data['main_image']);
			$content = file_get_contents($url);
			$fp = fopen("image/catalog/all_product_images/".trim($all_data['main_image']) , "w");
			fwrite($fp, $content);
			fclose($fp);
    }else{
    	 echo "error";
    }
}

function collect_data($detail_page){
if (isset(explode('<span class="modelNumber">',explode('</span><h2>',$detail_page)[0])[1])) {
$collect_data['modelnumber'] = explode('<span class="modelNumber">',explode('</span><h2>',$detail_page)[0])[1] ;

}else{
	$collect_data['modelnumber']="";
}
if (isset(explode('</span><h2><span>',explode('</span></h2></div>',$detail_page)[0])[1])) {
	$collect_data['shortdescription'] = explode('</span><h2><span>',explode('</span></h2></div>',$detail_page)[0])[1];
}else{
	$collect_data['shortdescription']="";
}
if (isset(explode('<div id="productView_frame1" class="productView_frame_cont"><ul>',explode("</ul></div><div class='product",$detail_page)[0])[1])) {
	$extra_image3 = explode('<div id="productView_frame1" class="productView_frame_cont">',explode("</div><div class='product",$detail_page)[0])[1];
	$collect_data['all_images'] = get_images($extra_image3);
	
}else{
	$collect_data['all_images'] = array();
}
if(!empty($collect_data['all_images'])){
		$collect_data['main_image'] = $collect_data['all_images'][0];
}else{
	if (isset(explode('<ul id="productView_content1" class="productView_content">',explode('</ul></div><div id="productView_thumbWrapper_cont">',$detail_page)[0])[1])) {
		$main_image3 = explode('<ul id="productView_content1" class="productView_content">',explode('</ul></div><div id="productView_thumbWrapper_cont">',$detail_page)[0])[1];
				$collect_data['main_image'] = get_images($main_image3);
				$collect_data['main_image'] = $collect_data['main_image'][0];
	}else{
		$collect_data['main_image'] = "photo_not_available.jpg";
	}
}
if (isset(explode('<div id="product_bullet_scroll">',explode('</div></div><div class="productView_rightcontainer">',$detail_page)[0])[1])) {
$collect_data['description'] = explode('<div id="product_bullet_scroll">',explode('</div></div><div class="productView_rightcontainer">',$detail_page)[0])[1];
}else{
	$collect_data['description']="";
}
if (isset(explode('<div class="descSection"><div class="productView_longDesc_box">',explode('</div> <div class="productView_contactText">',$detail_page)[0])[1])) {
$collect_data['additional_info'] = explode('<div class="descSection"><div class="productView_longDesc_box">',explode('</div> <div class="productView_contactText">',$detail_page)[0])[1];
}else{
	$collect_data['additional_info']="";
}
if (isset(explode('<div class="productDetail">Series Name:</div><div class="productDetail_text">',explode('</div><div class="productDetail">UPC / EAN:</div>',$detail_page)[0])[1])) {
$collect_data['series_name'] = explode('<div class="productDetail">Series Name:</div><div class="productDetail_text">',explode('</div><div class="productDetail">UPC / EAN:</div>',$detail_page)[0])[1];
}else{
	$collect_data['series_name']="";
}
if (isset(explode('<div class="productDetail">Brand Name</div><div class="productDetail_text">',explode('</div><div class="productDetail">Model Number:</div>',$detail_page)[0])[1])) {
$collect_data['brand_name'] = explode('<div class="productDetail">Brand Name</div><div class="productDetail_text">',explode('</div><div class="productDetail">Model Number:</div>',$detail_page)[0])[1];
}else{
	$collect_data['brand_name']="";
}
if (isset(explode('<div class="productView_spec"><a href="https://tmppro.com/specsheets/',explode('" target="_blank" rel="specSheet">',$detail_page)[0])[1])) {
$collect_data['specsheet'] = explode('<div class="productView_spec"><a href="https://tmppro.com/specsheets/',explode('" target="_blank" rel="specSheet">',$detail_page)[0])[1];
}else{
	$collect_data['specsheet']="";
}
if (isset(explode('<div class="wrap_price_map"><div>List: $',explode('</div><div>MAP:',$detail_page)[0])[1]) || isset(explode('<div class="wrap_price_map"><div>List: $',explode('</div><div>UPP:',$detail_page)[0])[1]) ) 
{
	if (isset(explode('<div class="wrap_price_map"><div>List: $',explode('</div><div>MAP:',$detail_page)[0])[1]))
	{
	$collect_data['msrp'] = explode('<div class="wrap_price_map"><div>List: $',explode('</div><div>MAP:',$detail_page)[0])[1];
	}else{
		$collect_data['msrp'] = explode('<div class="wrap_price_map"><div>List: $',explode('</div><div>UPP:',$detail_page)[0])[1];
	}
}elseif(isset(explode('<div class="productDetail">List:</div><div class="productDetail_text">$',explode('</div><div class="productDetail">Short',$detail_page)[0])[1])){
	$collect_data['msrp'] = explode('<div class="productDetail">List:</div><div class="productDetail_text">$',explode('</div><div class="productDetail">Short',$detail_page)[0])[1];
	
}else{
	$collect_data['msrp']=0;
}
if (isset(explode('</div><div>MAP: $',explode('</div></div><div class="wrap_price_inv">',$detail_page)[0])[1]) || isset(explode('</div><div>UPP: $',explode('</div></div><div class="wrap_price_inv">',$detail_page)[0])[1])) {
	if(isset(explode('</div><div>MAP: $',explode('</div></div><div class="wrap_price_inv">',$detail_page)[0])[1]))
	{
		$collect_data['map'] = explode('</div><div>MAP: $',explode('</div></div><div class="wrap_price_inv">',$detail_page)[0])[1];
	}else{
		$collect_data['map'] = explode('</div><div>UPP: $',explode('</div></div><div class="wrap_price_inv">',$detail_page)[0])[1];
	}
}else{
	$collect_data['map']=0;
}
if (isset(explode('<div class="wrap_price_right pv_oss"><div class="availabilityFieldNew"><p style="">Availability: ',explode('</span></p></div><p class="wrap_price_dealerprice',$detail_page)[0])[1])) {
$collect_data['stock'] = explode('<div class="wrap_price_right pv_oss"><div class="availabilityFieldNew"><p style="">Availability: ',explode('</span></p></div><p class="wrap_price_dealerprice',$detail_page)[0])[1];
	$collect_data['stock'] = strip_tags($collect_data['stock']);
	if($collect_data['stock'] == "In Stock"){
	 	$collect_data['stock'] = 1;
	 	$collect_data['qty'] = 100;
	 }else{
	 	$collect_data['stock'] = 0;
	 	$collect_data['qty'] = 0;
	 }
}else{
	$collect_data['stock'] = 0;
	$collect_data['qty'] = 0;
}
if (isset(explode('<b>Retail Package Measurements &amp; Shipping Weights</b></div>',explode('</div></div></div><div class="descSectionthree descSection">',$detail_page)[0])[1])) {
$collect_data['dimension'] = explode('<b>Retail Package Measurements &amp; Shipping Weights</b></div>',explode('</div></div></div><div class="descSectionthree descSection">',$detail_page)[0])[1];
	 $data = getTableData($collect_data['dimension']);
	 $collect_data['length'] = $data[2];
	 $collect_data['width'] = $data[3];
	 $collect_data['height'] = $data[4];
	 $collect_data['Weight'] = $data[5];
}else{
	 $collect_data['length'] = 0;
	 $collect_data['width'] = 0;
	 $collect_data['height'] = 0;
	 $collect_data['Weight'] = 0;
}
return $collect_data;
}
if (isset(explode('<div id="mainProductBoxWrapper">',explode('</div><div class="pagination_bot container">',$page)[0])[1])) {
$all_products_per_page_ids = explode('<div id="mainProductBoxWrapper">',explode('</div><div class="pagination_bot container">',$page)[0])[1];
		$all_products_per_page_ids = get_all_ids($all_products_per_page_ids);
		// print_r($all_products_per_page_ids); exit();
}else{
	$all_products_per_page_ids="";
}
if (isset(explode('Current Category: </span><span class="bold">',explode('</span><a href="https://tmppro.com/products/brand"',$page)[0])[1])) {
$category_name = explode('Current Category: </span><span class="bold">',explode('</span><a href="https://tmppro.com/products/brand"',$page)[0])[1];
		
		// print_r($category_name); exit();
}else{
	$category_name="";
}
$category_ids= array();
//$product = Mage::getModel('catalog/product');

/*$_category = Mage::getResourceModel('catalog/category_collection')
        ->addFieldToFilter('name', $category_name)
        ->load();
foreach ($_category as $cat) {
	# code...
	$category_ids[] = $cat->getId();
}*/
//$category_ids = implode(',',$category_ids);
// print_r($category_ids);exit();
function searchForId($value, $array) {
   foreach ($array as $key => $val) {
       if ($val['label'] === $value) {
           return $val['value'];
       }
   }
   return null;
}

//Possible color value
  /*$attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'product_brands'); //"color" is the attribute_code
  $allOptions = $attribute->getSource()->getAllOptions(true, true);*/
if (isset(explode('<div class="pagination_top container">',explode('</div><div id="mainProductBox" class="productContainer productMatrix container"',$page)[0])[1])) {
$page_no = explode('<div class="pagination_top container">',explode('</div><div id="mainProductBox" class="productContainer productMatrix container"',$page)[0])[1];
		//echo htmlspecialchars($page_no); exit();
		$page_no = substr_count($page_no,'<li>');
}else{
	$page_no = null;
}
/*print_r($page_no."</br>");
print_r(count($all_products_per_page_ids)); exit();*/
if($page_no != null || count($all_products_per_page_ids) > 0 )
{
	if($page_no > 0)
	{
		for($i=1; $i<=$page_no; $i++) 
		{
			$pagecurl = curl_init();
			curl_setopt($pagecurl, CURLOPT_FRESH_CONNECT, 1);
			curl_setopt($pagecurl, CURLOPT_URL, $url.'/page/'.$i);
			curl_setopt($pagecurl, CURLOPT_HEADER, 0);
			curl_setopt($pagecurl, CURLOPT_VERBOSE,1);
			curl_setopt($pagecurl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36');
			curl_setopt($pagecurl, CURLOPT_REFERER,'http://www.google.com');
			curl_setopt($pagecurl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($pagecurl, CURLOPT_POST,0);
			curl_setopt($pagecurl, CURLOPT_FOLLOWLOCATION, 20);
			curl_setopt($pagecurl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($pagecurl, CURLOPT_SSL_VERIFYHOST, 0);
			$strCookie = 'tmppro_sess=kco1l9rj5f4n7iq94vv372bknkehe90g';
			curl_setopt( $pagecurl, CURLOPT_COOKIE, $strCookie );
			$sub_page = curl_exec($pagecurl);
			curl_reset($pagecurl);
			get_all_ids($sub_page);

			if (isset(explode('<div id="mainProductBoxWrapper">',explode('</div><div class="pagination_bot container">',$sub_page)[0])[1]))
			{
				echo "<script>console.log( 'page no: " . $i . " starts' );</script>";

				$all_products_sub_page_ids = explode('<div id="mainProductBoxWrapper">',explode('</div><div class="pagination_bot container">',$sub_page)[0])[1];
				$all_products_sub_page_ids = get_all_ids($all_products_sub_page_ids);
			
			/*$all_products_sub_page_ids = array_splice($all_products_sub_page_ids,1,49);
			print_r($all_products_sub_page_ids); exit();*/
			
				if(count($all_products_sub_page_ids) > 0){
					foreach ($all_products_sub_page_ids as $detail_id) {

						$page_path ="https://tmppro.com/products/view/".trim($detail_id);

						get_product_images($page_path);
					}
				}
				
			}
			echo "<script>console.log( 'page no: " . $i . " end' );</script>";
		curl_close($pagecurl);
		}
	}else{
	// echo "else"; exit();
		if(count($all_products_per_page_ids) > 0)
		{
			// echo count($all_products_per_page_ids); exit();
			foreach ($all_products_per_page_ids as $detail_id)
			{
						$detail_page = "https://tmppro.com/products/view/".trim($detail_id);
						$detailpagecurl = curl_init($detail_page);
						curl_setopt($detailpagecurl, CURLOPT_HEADER, 0);
                        curl_setopt($detailpagecurl, CURLOPT_VERBOSE, 1);
                        curl_setopt($detailpagecurl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36');
                        curl_setopt ($detailpagecurl, CURLOPT_REFERER,'http://www.google.com'); 
                        curl_setopt($detailpagecurl, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($detailpagecurl,CURLOPT_POST,0);
                        curl_setopt($detailpagecurl, CURLOPT_FOLLOWLOCATION, 20);
                        curl_setopt($detailpagecurl,CURLOPT_SSL_VERIFYPEER, false);
                        $strCookie = 'tmppro_sess=kco1l9rj5f4n7iq94vv372bknkehe90g';
						curl_setopt( $detailpagecurl, CURLOPT_COOKIE, $strCookie ); 
						$detail_page = curl_exec($detailpagecurl);
						$all_data = collect_data($detail_page);
						 
							
				if (!empty($all_data['all_images'])) 
				{
	        		foreach ($all_data['all_images'] as $additional_image) 
	        		{
			            $url = "https://tmppro.com/productImage/".trim($additional_image);
						$content = file_get_contents($url);
						$fp = fopen("image/catalog/all_product_images/".trim($additional_image) , "w");
						fwrite($fp, $content);
						fclose($fp);
			        }
	    		}elseif(!empty($all_data['main_image']))
	    		 {
				    	$url = "https://tmppro.com/productImage/".trim($all_data['main_image']);
						$content = file_get_contents($url);
						$fp = fopen("image/catalog/all_product_images/".trim($all_data['main_image']) , "w");
						fwrite($fp, $content);
						fclose($fp);
			    }else{
			    	 echo "error";
			    }
							
			}
			echo "<script>console.log( 'single page done' );</script>";
		}
	}
}
echo "done"; exit()
?>